const googleMapsClient = require("@google/maps").createClient({
  key: "AIzaSyB-1dIuVyxzgprEO5-9e8bdarOzlBH40K4",
  Promise: Promise
});

const getLatLng = async address => {
  if (!address) return null;
  try {
    console.log("Getting geo for : ", address);
    let result = await googleMapsClient.geocode({ address }).asPromise();
    // console.log("Result: ", result.json.results);
    if (result.json.results.length > 0) {
      let loc = result.json.results[0].geometry.location;
      return { latitude: loc.lat, longitude: loc.lng };
    }
    return null;
  } catch (e) {
    console.error(e);
  }
  return null;
};
module.exports = { getLatLng };
