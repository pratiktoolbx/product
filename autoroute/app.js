// const cheerio = require('cheerio')
// const axios = require("axios");
const fs = require("fs").promises;

const salesforce = require("./salesforce");
const geocode = require("./geocode");
const batch = require("./batch");

let drivers = [
  {
    name: "A",
    start: 7,
    end: 15,
    capacity: 1200,
    busyHour: null,
  },
  {
    name: "B",
    start: 7,
    end: 15,
    capacity: 1200,
    busyHour: null,
  },
  {
    name: "C",
    start: 7,
    end: 15,
    capacity: 1500,
    busyHour: null,
  },
  {
    name: "D",
    start: 7,
    end: 15,
    capacity: 1500,
    busyHour: null,
  },
  {
    name: "E",
    start: 7,
    end: 15,
    capacity: 1200,
    busyHour: null,
  },
  {
    name: "F",
    start: 7,
    end: 15,
    capacity: 1200,
    busyHour: null,
  },
  {
    name: "G",
    start: 7,
    end: 15,
    capacity: 1200,
    busyHour: null,
  },
  {
    name: "Sprinter Van",
    start: 7,
    end: 17,
    capacity: 3250,
    busyHour: null,
  },
  {
    name: "Sprinter Van",
    start: 7,
    end: 15,
    capacity: 3250,
    busyHour: null,
  },
];

let scheduledOrderQueue = [];
let sameDayOrderQueue = [];
let availableDrivers = drivers.length;
let busyDrivers = [];

let totalBatchedRuns = 0;
let totalBatchedOrders = 0;
function compareConfirmationDates(a, b) {
  // Use toUpperCase() to ignore character casing
  const dateA = a.confirmedDate;
  const dateB = b.confirmedDate;
  const hourA = a.confirmedHour;
  const hourB = b.confirmedHour;

  return dateA === dateB ? hourA > hourB : dateA > dateB;
}

var readOrders = async () => {
  let orders = [];
  try {
    let ordersFromFile = await fs.readFile("./orders.json", "utf-8");

    if (ordersFromFile.length > 0) {
      orders = JSON.parse(ordersFromFile);
    }
  } catch (e) {
    console.error(e);
  }

  for (let o of orders) {
    o.Total_Weight__c = setWeightBasedOnSize(o);
    console.log(
      `${o.OrderNumber}|${o.Size__c}|${o.Delivery_Type__c}|${o.Drop_Off_Address__c}`
    );
  }
  // console.log("Orders: ", orders);
  await salesforce.connect();
  let orderObj = await salesforce.getOrdersToday();
  // let orderObj = { records: [] };

  for (let o of orderObj.records) {
    let existingOrder = orders.find((x) => x.OrderNumber === o.OrderNumber);
    // console.log("Existing ORder: ", existingOrder);
    if (existingOrder) {
      o.pLatitude = existingOrder.pLatitude;
      o.pLongitude = existingOrder.pLongitude;
      o.dLatitude = existingOrder.dLatitude;
      o.dLongitude = existingOrder.dLongitude;
    }
    // console.log("Processing: ", o.OrderNumber, o.pLatitude, o.dLatitude);

    if (!o.pLatitude) {
      let pLoc = await geocode.getLatLng(o.Pickup_Location_Address__c);
      if (pLoc) {
        o.pLatitude = pLoc.latitude;
        o.pLongitude = pLoc.longitude;
      }
    }
    if (!o.dLatitude) {
      let dLoc = await geocode.getLatLng(o.Drop_Off_Address__c);
      if (dLoc) {
        o.dLatitude = dLoc.latitude;
        o.dLongitude = dLoc.longitude;
      }
    }
    o.Total_Weight__c = setWeightBasedOnSize(o);

    let orderIndex = orders.findIndex((x) => x.OrderNumber === o.OrderNumber);
    if (orderIndex > -1) {
      orders.splice(orderIndex, 1, o);
    } else {
      orders.push(o);
    }
  }

  await fs.writeFile("./orders.json", JSON.stringify(orders));
  return orders;
  // let csv = await fs.readFile("./orders.csv", "utf-8");
  // let data = await neatCsv(csv);
  // for (let o of data) {

  // }

  // return orders.sort(compareConfirmationDates);
};

var isTBSelect = async (order) => {
  return order.Type == "TOOLBX Select";
};

var canOrderWait = async (order, currentHour) => {
  // deliveryType == Same Day
  // deliveryType == Scheduled && deliveryHour - currentHour > 1

  if (order.Delivery_Type__c === "Same Day") return true;

  if (
    order.Delivery_Type__c === "Scheduled" &&
    order.Delivery_Hour__c - currentHour > 1
  ) {
    return true;
  }

  return false;
};

var resetDay = () => {
  for (let d of drivers) {
    d.busyHour = null;
  }
};
var checkForAvailableDrivers = (currentHour) => {
  let availableCount = 0;
  for (let d of drivers) {
    if (currentHour - d.busyHour > 1) {
      // freed up
      availableCount = availableCount + 1;
      d.busyHour = null;
    } else {
      busyDrivers.push(d);
    }
  }
};
var findAvailableDriver = (order, currentHour) => {
  // TODO: take into account last known location
  let weight = order.Total_Weight__c;

  for (let d of drivers) {
    // console.log(
    //   `Driver check: ${d.name} capicity: ${d.capacity} and weight of material: ${weight}`
    // );
    if (
      currentHour >= d.start &&
      currentHour <= d.end &&
      (d.busyHour === null || currentHour - d.busyHour > 1)
    ) {
      if (d.capacity > weight) {
        d.busyHour = currentHour;
        // busyDrivers.push(d);
        return d;
      }
    }
  }
  //   console.log("Could not find available driver for this hour: ", currentHour);
  return null;
};

var setWeightBasedOnSize = (order) => {
  if (
    order.Total_Weight__c === "" ||
    order.Total_Weight__c === undefined ||
    parseInt(order.Total_Weight__c) < 2 ||
    isNaN(parseInt(order.Total_Weight__c))
  ) {
    if (order.Size__c === "Small") {
      return 150;
    } else if (order.Size__c === "Medium") {
      return 800;
    } else if (order.Size__c === "Large") {
      return 2000;
    }
  }
  return order.Total_Weight__c;
};

const batchSameDays = (order, driver) => {
  let batchedOrders = [];
  let weight = order.Total_Weight__c;
  let list = sameDayOrderQueue.filter(
    (o) => o.Drop_Off_Address__c !== order.Drop_Off_Address__c
  );
  let sameAddressOrders = batch.getAddressMatches(order, weight, list);

  batchedOrders.push(...sameAddressOrders.matched);
  sameDayOrderQueue = [...sameAddressOrders.unmatched];

  weight = sameAddressOrders.weight;
  let sameDayOrders = batch.getProximityMatches(
    order,
    weight,
    driver,
    sameDayOrderQueue
  );
  batchedOrders.push(...sameDayOrders.matched);
  sameDayOrderQueue = [...sameDayOrders.unmatched];
  weight = sameDayOrders.weight;
  return batchedOrders;
};

var findOrdersToBatch = (currentHour, driver, mainOrder) => {
  let weight = mainOrder.Total_Weight__c;

  let batchedOrders = [];

  let matchedOrders = batch.getAddressMatches(
    mainOrder,
    weight,
    sameDayOrderQueue
  );
  batchedOrders.push(...matchedOrders.matched);
  sameDayOrderQueue = [...matchedOrders.unmatched];
  weight = matchedOrders.weight;

  matchedOrders = batch.getProximityMatches(
    mainOrder,
    weight,
    driver,
    sameDayOrderQueue
  );
  batchedOrders.push(...matchedOrders.matched);
  sameDayOrderQueue = [...matchedOrders.unmatched];
  weight = matchedOrders.weight;

  matchedOrders = batch.getScheduledMatches(
    mainOrder,
    weight,
    currentHour,
    driver,
    scheduledOrderQueue
  );

  batchedOrders.push(...matchedOrders.matched);
  scheduledOrderQueue = [...matchedOrders.unmatched];
  weight = matchedOrders.weight;

  return batchedOrders;
};

var sendOrdersToDriver = async (
  currentHour,
  driver,
  mainOrder,
  batchedOrders
) => {
  driver.busyHour = currentHour + 1;
  availableDrivers = 0;
  for (let d of drivers) {
    if (d.busyHour === null) {
      availableDrivers = availableDrivers + 1;
    }
  }
};
// Main

var batchAndRouteOrder = (currentHour, order) => {
  let driver = findAvailableDriver(order, currentHour);

  if (!driver) {
    scheduledOrderQueue.push(order);
  } else {
    let batchedOrders;
    if (order.Delivery_Type__c === "Same Day") {
      batchedOrders = batchSameDays(order, driver);
    } else {
      batchedOrders = findOrdersToBatch(currentHour, driver, order);
    }

    // console.log("Batched: ", batchedOrders.length);
    sendOrdersToDriver(currentHour, driver, order, batchedOrders);

    if (batchedOrders.length > 0) {
      totalBatchedRuns = totalBatchedRuns + 1;
      totalBatchedOrders = totalBatchedOrders + batchedOrders.length + 1;
      console.log("==========" + order.OrderNumber + "==========");
      console.log(`Batched an order with ${batchedOrders.length + 1} orders`);
      console.log(
        `${order.OrderNumber} | ${order.Type} | ${order.Delivery_Type__c} | ${order.Size__c} | ${order.Total_Weight__c} | ${order.Pickup_Location_Address__c} | ${order.Drop_Off_Address__c}`
      );
      for (let o of batchedOrders) {
        console.log(
          `${o.OrderNumber} | ${o.Type} | ${o.Delivery_Type__c} | ${
            o.Size__c
          } | ${o.Total_Weight__c} |${o.Pickup_Location_Address__c} | ${
            o.Drop_Off_Address__c
          } | PU Distance: ${batch.getDistance(
            order,
            o,
            "pickup"
          )} | DO Distance: ${batch.getDistance(order, o, "dropoff")}`
        );
      }
    } else {
      // console.log("==========" + order.OrderNumber + "==========");
      // console.log(
      //   `${order.OrderNumber} | ${order.Type} | ${order.Delivery_Type__c} | ${order.Size__c} | ${order.Total_Weight__c} | ${order.Pickup_Location_Address__c} | ${order.Drop_Off_Address__c}`
      // );
    }
  }
};

const preAssigned = (order) => {
  if (
    order.Status === "Paid" ||
    order.Status === "Invoiced" ||
    order.Status === "Delivered" ||
    order.Status === "In Progress"
  ) {
    return false;
  }
  return true;
};
(async () => {
  try {
    readOrders().then((orders) => {
      console.log("Total Orders: ", orders.length);

      const expressOrders = orders.filter(
        (o) => o.Delivery_Type__c === "Express" && preAssigned(o)
      );
      sameDayOrderQueue = orders.filter(
        (o) => o.Delivery_Type__c === "Same Day" && preAssigned(o)
      );
      scheduledOrderQueue = orders.filter(
        (o) => o.Delivery_Type__c === "Scheduled" && preAssigned(o)
      );

      console.log("Express Orders: ", expressOrders.length);
      console.log("Same Day Orders: ", sameDayOrderQueue.length);
      console.log("Scheduled Orders: ", scheduledOrderQueue.length);
      for (let o of expressOrders) {
        batchAndRouteOrder(8, o);
      }

      // console.log("Same Day Orders: ", sameDayOrderQueue.length);
      // console.log("Scheduled Orders: ", scheduledOrderQueue.length);
      for (let hour = 0; hour < 16; hour++) {
        let activeOrders = scheduledOrderQueue.filter(
          (o) => o.Delivery_Hour__c === hour
        );

        // console.log("---- For: " + hour + ":00 --------");

        for (let o of activeOrders) {
          batchAndRouteOrder(hour, o);
        }
      }

      console.log("Same Days ==========", sameDayOrderQueue.length);
      for (let o of sameDayOrderQueue) {
        batchAndRouteOrder(12, o);
      }
      console.log("Total Batched Runs: ", totalBatchedRuns);
      console.log("Total Batched Orders: ", totalBatchedOrders);
      console.log("Total Orders: ", orders.length);
      console.log("Total Drivers: ", drivers.length);
      // console.log("Undelivered Same Days: ", sameDayOrderQueue.length);

      // console.log(
      //   "Total Undelivered Orders: ",
      //   sameDayOrderQueue.length + scheduledOrderQueue.length
      // );

      // for (let o of sameDayOrderQueue) {
      //   console.log(
      //     `SameDay -- weight: ${o.weight} day: ${o.confirmedDate} hour: ${o.confirmedHour}`
      //   );
      // }
      // for (let o of scheduledOrderQueue) {
      //   console.log(
      //     `Scheduled -- weight: ${o.weight} day: ${o.confirmedDate} hour: ${o.confirmedHour}`
      //   );
      // }
    });
  } catch (e) {
    console.log("error: ", e);
  }
})();
