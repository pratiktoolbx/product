const geodist = require("geodist");

const getDistance = (a, b, type) => {
  let km = 0;
  let aLoc, bLoc;
  if (type === "pickup") {
    aLoc = { lat: a.pLatitude, lon: a.pLongitude };
    bLoc = { lat: b.pLatitude, lon: b.pLongitude };
  } else if (type === "dropoff") {
    aLoc = { lat: a.dLatitude, lon: a.dLongitude };
    bLoc = { lat: b.dLatitude, lon: b.dLongitude };
  }
  km = geodist(aLoc, bLoc, { exact: true, unit: "km" });
  return km;
};

var isPickupInProximity = (a, b) => {
  if (a.Pick_Up_Location__c === b.Pick_Up_Location__c) {
    return true;
  }

  let km = getDistance(a, b, "pickup");

  return km < 4;
};

var isDropOffInProximity = (a, b) => {
  if (a.Drop_Off_Address__c === b.Drop_Off_Address__c) {
    return true;
  }
  let km = getDistance(a, b, "dropoff");
  return km < 6;
};

const genericFilters = (order, o) => {
  if (o.OrderNumber === order.OrderNumber) return false;
  if (
    o.Material__c !== undefined &&
    o.Material__c !== null &&
    o.Material__c.indexOf("16'") > -1
  )
    return false;
  return true;
};

const getAddressMatches = (order, weight, compareList) => {
  let unmatched = [];
  let matched = compareList.filter(o => {
    if (!genericFilters(order, o)) return false;
    if (o.Drop_Off_Address__c === order.Drop_Off_Address__c) {
      return true;
    } else {
      unmatched.push(o);
      return false;
    }
  });

  return { matched, unmatched, weight };
};

const getProximityMatches = (order, weight, driver, list) => {
  let unmatched = [];
  let matched = list.filter(o => {
    if (!genericFilters(order, o)) return false;
    if (
      weight + o.Total_Weight__c < driver.capacity &&
      isPickupInProximity(order, o) &&
      isDropOffInProximity(order, o)
    ) {
      weight = weight + o.Total_Weight__c;
      return true;
    } else {
      unmatched.push(o);
      return false;
    }
  });
  return { matched, unmatched, weight };
};

const getScheduledMatches = (order, weight, currentHour, driver, list) => {
  let unmatched = [];
  let matched = list.filter(o => {
    if (!genericFilters(order, o)) return false;
    if (
      o.Delivery_Hour__c - currentHour < 2 &&
      isPickupInProximity(o, order) &&
      isDropOffInProximity(o, order) &&
      weight + o.Total_Weight__c < driver.capacity
    ) {
      weight = weight + o.Total_Weight__c;
      return true;
    } else {
      unmatched.push(o);
      return false;
    }
  });
  return { matched, unmatched, weight };
};

module.exports = {
  getDistance,
  getAddressMatches,
  getProximityMatches,
  getScheduledMatches
};
