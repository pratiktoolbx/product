const jsforce = require("jsforce");

const SALESFORCE_CLIENT_ID =
  "3MVG9mclR62wycM1XWyIyabwsHWslBRdlrFX4IYAOS7gfGNTmC_6_4JsYpE1D.pOFd_2tQIieI86w9dricpSf";
const SALESFORCE_CLIENT_SECRET = "1297804818503699477";
const SALESFORCE_INSTANCE_URL = "https://toolbx.my.salesforce.com";
const SALESFORCE_LOGIN_URI = "https://login.salesforce.com";
const SALESFORCE_PASSWORD =
  "X@9HqEc7x7BeDZm*kNy9w58E36W&Gr$D&jn&&UcCzMxuNKZqkX";
const SALESFORCE_REDIRECT_URI =
  "https://api.toolbxops.net/salesforce/oauth2/callback";
const SALESFORCE_USERNAME = "dev@toolbx.com";

let conn = new jsforce.Connection({
  oauth2: {
    clientId: SALESFORCE_CLIENT_ID,
    clientSecret: SALESFORCE_CLIENT_SECRET,
    loginUrl: SALESFORCE_LOGIN_URI,
    redirectUri: SALESFORCE_REDIRECT_URI,
  },
});
conn.bulk.pollTimeout = 60000;

const connect = async () => {
  try {
    await conn.login(SALESFORCE_USERNAME, SALESFORCE_PASSWORD);
    console.log("Logged In: ", conn.accessToken);
  } catch (e) {
    console.log("Exception when trying to login: ", e);
  }
  return null;
};

const getOrdersToday = async () => {
  try {
    let orders = await conn.query(
      `SELECT 
        OrderNumber,
        Type,
        AccountId,
        EffectiveDate,
        Size__c,
        Status,
        Subscription__c,
        Delivery_Type__c,
        Unloading_Option__c,
        Delivery_Hour__c,
        Total_Weight__c,
        Pick_Up_Location__c,
        Pickup_Location_Address__c,
        Drop_Off_Address__c,
        Drop_Off_Location__c,
        Material__c 
      FROM 
        Order 
      Where 
        EffectiveDate = 2020-04-17`
    );

    console.log("Order length: ", orders.records.length);
    return orders;
  } catch (e) {
    console.error(e);
  }
  return null;
};

module.exports = {
  connect,
  getOrdersToday,
};
