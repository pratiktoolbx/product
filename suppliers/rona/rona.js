const cheerio = require("cheerio");
const puppeteer = require("puppeteer-extra");
const output = require("./output");
const db = require("./src/db/db");
const { performance } = require("perf_hooks");
const cliProgress = require("cli-progress");
// const AdblockerPlugin = require("puppeteer-extra-plugin-adblocker");
// const StealthPlugin = require("puppeteer-extra-plugin-stealth");
// const pluginStealth = StealthPlugin();
// pluginStealth.enabledEvasions.delete("accept-language");

// puppeteer.use(StealthPlugin());
// puppeteer.use(AdblockerPlugin({ blockTrackers: true }));
// puppeteer.use(
//   require("puppeteer-extra-plugin-block-resources")({
//     blockedTypes: new Set(["images", "stylesheet", "font"]),
//   })
// );
// parsing effectively: https://levelup.gitconnected.com/anonymous-web-scrapping-with-node-js-tor-apify-and-cheerio-3b36ec6a45dc

function sleep(milliseconds) {
  const date = Date.now();
  let currentDate = null;
  do {
    currentDate = Date.now();
  } while (currentDate - date < milliseconds);
}

const changeStore = async (page, postalCode) => {
  await Promise.all([
    page.goto("https://www.rona.ca/en/find-a-store"),
    page.waitForNavigation({ waitUntil: "networkidle0" }),
  ]);

  const addressElement = await page.$(".storelocator__info-address");
  const addressText = await page.evaluate(
    (addressElement) => addressElement.textContent,
    addressElement
  );

  if (addressText.indexOf(postalCode) > -1) {
    console.log("already at store", addressText.replace(/^\s+/g, ""));
    return;
  }

  // console.log("Adding postal: ", postalCode);
  await page.type("#storeLocAddress", postalCode, { delay: 20 });

  // console.log("Clicking submit");

  await Promise.all([
    page.click(".storelocator__form-button"),
    page.waitForNavigation({ waitUntil: ["load", "networkidle0"] }),
  ]);
  // console.log("Delay One");
  sleep(2000);
  // console.log("Clicking Select Store");
  const element = await page.$(".storelocator__info-title");
  const text = await page.evaluate((element) => element.textContent, element);
  console.log("Store: ", text.replace(/^\s+/g, ""));

  // const element = await page.$(".storelocator__item:nth-child(0n+1)");
  // const text = await page.evaluate((element) => element.textContent, element);
  // console.log("Store: ", text);

  await Promise.all([
    page.evaluate(() => {
      document.querySelector(".js-selectThisStore").click();
    }),
    // page.click(".js-selectThisStore"),
    page.waitForNavigation({ waitUntil: ["load", "networkidle0"] }),
  ]);
  sleep(2000);
  // await page.click(".js-selectThisStore");
  console.log("Store changed");
  // sleep(3000);
  // await page.waitForNavigation({ waitUntil: "networkidle0" });
  // console.log("Page loaded");
  return;
};

const pullInfoFromContent = async (content) => {
  //   let response = await axios.get(url);
  let $ = await cheerio.load(content);

  let store = $(".widget__storeInfo-trigger")
    .text()
    .match(/RONA.+/)[0];
  let title = $(".productDetails > .page-product__title").text();

  let sku = $(".page-product__sku-infos > span").eq(0).text().match(/\d+/)[0];
  let price = $(".product_price_container")
    .text()
    .replace(/\s/g, "")
    .match(/[^a-zA-z]+/)[0];
  // let description = $("[itemprop='description']").text();

  let inventory = $(".page-product__inventory__unit")
    .eq(0)
    .text()
    .replace(/\s/g, "")
    .match(/\d+/);

  let stock = inventory ? parseInt(inventory[0]) : -1;

  // console.log("stock: ", stock);

  return {
    store,
    title,
    sku,
    price,
    stock,
  };
};

(async () => {
  console.log("starting: ");
  const t0 = performance.now();

  const browser = await puppeteer.launch({
    headless: true,
  });

  const page = await browser.newPage();

  let postals = {
    stockyards: "M6N 5B7",
    sheppard: "M2N 3B1",
    martingrove: "M9W 4X1",
  };

  let master = {};

  let count = 0;

  for (const [supplierStore, postalCode] of Object.entries(postals)) {
    console.log(`${supplierStore}: ${postalCode}`);

    await changeStore(page, postalCode);

    const products = await db.getCurrentProductList();
    console.log("Product length: ", products.length);
    // create a new progress bar instance and use shades_classic theme

    let progressBar = new cliProgress.SingleBar(
      {},
      cliProgress.Presets.shades_classic
    );
    progressBar.start(products.length, 0);

    for (let p of products) {
      try {
        count = count + 1;
        progressBar.update(count);
        let url = p.sku;

        // let urls = [
        //   "https://www.rona.ca/en/cobra-concrete-screws-683t-04845574",
        //   "https://www.rona.ca/en/venmar-installation-kit-for-air-exchanger-ea20130-0909052",
        //   "https://www.rona.ca/en/fuller-wood-chisel-300-0955-1422113",
        // ];

        // for (let url of urls) {
        await Promise.all([
          page.goto(url),
          page.waitForNavigation({ waitUntil: "networkidle0" }),
        ]);
        let content = await page.content();

        if (master[url] === undefined) {
          master[url] = {};
        }

        master[url][supplierStore] = await pullInfoFromContent(content, page);
      } catch (e) {
        console.log(`Couldn't get product ${p.id}, ${p.name}: ${p.sku}`);
      }
    }
    progressBar.stop();
  }

  const t1 = performance.now();
  console.log("Scraping took " + (t1 - t0) / 1000 + " seconds.");
  output.writeStockToCSV(master);

  browser.close();
})();
