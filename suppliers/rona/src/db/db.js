const psql = require("./postgres");
const sql = psql.sql;

const getCurrentProductList = async () => {
  try {
    console.log("Getting Current Product List From Select...");
    const products = [];
    let records = await sql`SELECT distinct("p"."id"),"p"."name","p"."price","p"."sku","p"."isActive" from "supplier" "s" join "category" "c" on "c"."supplierId" = "s"."id" join "product_categories_category" "pcc" on "c"."id"="pcc"."categoryId" join "product" "p" on "pcc"."productId"="p"."id" where "s"."id" in ('f9c45f9d-497b-4037-b683-3d233c47bbed') and "p"."isActive"=true`;
    // let records = await sql`SELECT distinct("p"."id"),"p"."name","p"."price","p"."sku","p"."isActive" from "supplier" "s" join "category" "c" on "c"."supplierId" = "s"."id" join "product_categories_category" "pcc" on "c"."id"="pcc"."categoryId" join "product" "p" on "pcc"."productId"="p"."id" where "s"."id" in ('c79b3940-0e60-4025-b996-559d6fa2e8b6') and "p"."id"='0fa474c6-d116-4d68-a73f-4f5ddd4a802d'`;

    if (records.count > 0) {
      for (let r of records) {
        products.push({
          id: r.id,
          name: r.name,
          current: r.price,
          sku: r.sku,
        });
      }
    } else {
      console.log("No records found");
    }
    return products;
  } catch (e) {
    console.log("Couldn't get current products: ", e);
    return {};
  }
};

module.exports = {
  getCurrentProductList,
};
