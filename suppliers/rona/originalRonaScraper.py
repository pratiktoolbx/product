from selenium import webdriver
import datetime
from bs4 import BeautifulSoup
import os, time, math, csv
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import WebDriverException
from selenium.common.exceptions import TimeoutException
from selenium.common.exceptions import StaleElementReferenceException
from selenium.webdriver.common.keys import Keys
import selenium.webdriver.support.ui as ui
from selenium.webdriver.support.ui import Select
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

PROJECT_PATH = os.getcwd()
PROJECT_PATH = PROJECT_PATH.replace("\\",'/')


def write_csv(data):
    file_exists = os.path.isfile("data.csv")
    with open("data.csv", 'a', newline='', encoding='utf-8-sig') as f:
        writer = csv.writer(f, delimiter=',')
        if not file_exists:
            writer.writerow(('url', 'name', 'main_category_name', 'category_name', 'sku', 'img', 'price', 'unit', 'Product_specifications', 'description'))
        writer.writerow((data['url'], data['name'], data['main_category_name'], data['category_name'], data['sku'], data['img'], data['price'], data['unit'], data['Product_specifications'], data['description']))                                                                                       


def write_csv_categories(data):
    file_exists = os.path.isfile("categories.csv")
    with open("categories.csv", 'a', newline='', encoding='utf-8-sig') as f:
        writer = csv.writer(f, delimiter=',')
        if not file_exists:
            writer.writerow(('Category Name', 'Parent Category Name', 'Category URL'))
        writer.writerow((data['Category_Name'], data['Parent_Category_Name'], data['Category_URL']))                                                                                       


def read_keywords():
    with open('input2.txt', 'r') as f:
        return f.readlines()

def collect_data():
    # time.sleep(100)
    chromeOptions = webdriver.ChromeOptions()
    prefs = {"profile.managed_default_content_settings.images":2,
    "profile.default_content_setting_values.notifications" : 2}
    chromeOptions.add_argument("--incognito")
    # chromeOptions.add_argument("--headless")
    chromeOptions.add_experimental_option("prefs",prefs)
    browser = webdriver.Chrome(chrome_options=chromeOptions,executable_path=PROJECT_PATH+'/chromedriver')

    # browser = webdriver.Chrome(chrome_options=chromeOptions)
    wait = ui.WebDriverWait(browser,30)
    browser.get('https://www.rona.ca/en/all-products')
    wait.until(lambda browser: browser.find_element_by_css_selector('.page-allproduct__box'))
    soup = BeautifulSoup(browser.page_source, "lxml")
    # browser.get('https://fugamusic.com/catalog?top_menu_item=true')
    # wait.until(lambda browser: browser.find_element_by_css_selector('#catalogue > table'))
    # wait.until(lambda browser: browser.find_element_by_css_selector('#search_results_number'))
    cat1 = soup.find('div', class_='page-allproduct').find_all('div', class_='page-allproduct__box')
    del cat1[-1]
    # cat1_list_text=[]
    urls = []
    cate = []
    for item in cat1:
        url_list = item.find_all('a', {'class': 'page-allproduct__box__link'})
        for url in url_list:
            urls.append(url.get('href'))
            data2 = {
                'Parent_Category_Name': item.find('a', class_='page-allproduct__box__title').get('data-name'),
                # 'cat1_url': item.find('a', class_='page-allproduct__box__title').get('href'),
                'Category_Name': url.get('data-name'),
                'Category_URL': url.get('href'),
            }
            cate.append(data2)
            write_csv_categories(data2)

    # print(cate[0]['cat1'])
    # time.sleep(100)
    # for url in urls:
    #     print(url)
    # time.sleep(100)
    j=0
    # print(page_count)
    # urls=read_keywords()
    while j < len(urls):

        browser.get(urls[j].strip()+'?pageSize=96')
        try:
            wait.until(lambda browser: browser.find_element_by_css_selector('div.holder div.product-tile'))
        except TimeoutException:
            print('cat error')
            j+=1
            continue
        soup = BeautifulSoup(browser.page_source, "lxml")
        holder = soup.find('div', class_='holder')
        list = holder.find_all('div', class_='product-tile')
        page_count = holder.get('data-page-count')
        i=0        
        while i < int(page_count):
            if i != 0:
                browser.get(urls[j].strip()+'/'+str(i+1)+'?pageSize=96')
                wait.until(lambda browser: browser.find_element_by_css_selector('div.product-tile'))
                soup = BeautifulSoup(browser.page_source, "lxml")
                holder = soup.find('div', class_='holder')
                list = holder.find_all('div', class_='product-tile')
            p=0
            for item in list:
                item_url = item.find('a', class_='productLink').get('href')
                browser.get(item_url)
                wait.until(lambda browser: browser.find_element_by_css_selector('h1'))
                soup = BeautifulSoup(browser.page_source, "lxml")
                name = soup.find('h1', class_='page-product__title').text.strip()

                sku = item.get('data-sku')
                img_url = item.find('img', class_='product-tile__image').get('src').replace('_M.jpg','_L.jpg')
                try:
                    price = item.find('span', class_='price-box__price__amount').text.replace('\n','').strip()
                except:
                    price = ''
                breadcrumb = soup.find('div', id='breadcrumb').find_all('a')
                main_category_name = breadcrumb[2].text.strip()
                category_name = breadcrumb[len(breadcrumb)-1].text.strip()
                # if float(item.get('data-price-regular')) != float(price):
                #     old_price = item.get('data-price-regular')
                # else:
                #     old_price = ''
                # brand = item.find('div', class_='product-tile__brand').text.strip()
                try:
                    unit = soup.find('span', class_='price-box__price__spec').text.strip()
                except:
                    unit = ''
                try:
                    spec_list = soup.select_one('div.page-product__specs.clearfix').select('div.row.row--no-padding.row--flex-full-height.row--full-width')
                    Product_specifications = ''
                    for spec in spec_list:
                        spec_name = spec.find('div', class_='page-product__specs__name').text.strip()
                        spec_value = spec.find('div', class_='page-product__specs__value').text.strip()
                        Product_specifications = Product_specifications + spec_name + ': ' + spec_value + '; '
                except:
                    Product_specifications = ''
                description = ''
                try:
                    description = soup.find('div', {'itemprop':'description'}).text.strip()
                except:
                    pass
                
                data = {
                    'url': item_url,
                    'name': name,
                    # 'main_category_name': cate[j]['cat1'],
                    # 'category_name': cate[j]['cat2'],
                    'main_category_name': main_category_name,
                    'category_name': category_name,
                    'sku': sku,
                    'img': img_url,
                    'price': price,
                    # 'old_price': old_price,
                    'unit': unit,
                    # 'brand': brand,
                    'Product_specifications': Product_specifications,
                    'description': description,
                    # 'Related_products': Related_products,
                    # 'img_products': img_products,
                    # 'Unit': Unit,
                }
                write_csv(data)   
                # if p==0:
                #     itemListElements = soup.find('div', class_='m-breadcrumbs-col').find('ol').find_all('li')
                #     if len(itemListElements)==6:
                #         data2 = {
                #             'cat1': main_category_name,
                #             'cat1_url': 'https://www.hilti.ca' + itemListElements[2].find('a').get('href'),
                #             'cat2': itemListElements[3].text.strip(),
                #             'cat2_url': 'https://www.hilti.ca' + itemListElements[3].find('a').get('href'),
                #             'cat3': itemListElements[4].text.strip(),
                #             'cat3_url': 'https://www.hilti.ca' + itemListElements[4].find('a').get('href'),
                #         }
                #     else:
                #         data2 = {
                #             'cat1': main_category_name,
                #             'cat1_url': 'https://www.hilti.ca' + itemListElements[2].find('a').get('href'),
                #             'cat2': itemListElements[3].text.strip(),
                #             'cat2_url': 'https://www.hilti.ca' + itemListElements[3].find('a').get('href'),
                #             'cat3': '',
                #             'cat3_url': '',
                #         }
                #     write_csv_categories(data2)
                print(main_category_name)
                print(category_name)
                print(str(j+1)+'/'+str(len(urls)))
                print(str(i+1)+'/'+page_count)
                print(str(p+1)+'/'+str(len(list)))
                print('')
                p+=1
                time.sleep(1.5)
            i+=1
        j+=1 

    browser.quit()


if __name__ == '__main__':
    collect_data()
