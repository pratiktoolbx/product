const fs = require("fs");

const RONA_STOCKYARDS = "stockyards";
const RONA_SHEPPARD = "sheppard";
const RONA_MARTINGROVE = "martingrove";

const writeStockToCSV = (info) => {
  // Update File
  let filename = "./stock.csv";
  fs.writeFileSync(filename, "");
  fs.appendFileSync(filename, `url, ${RONA_STOCKYARDS}, ${RONA_SHEPPARD}`);
  for (let u of Object.keys(info)) {
    // console.log(info[u]);
    try {
      fs.appendFileSync(filename, "\n");

      fs.appendFileSync(
        filename,
        [
          u,
          info[u][RONA_STOCKYARDS].stock,
          info[u][RONA_SHEPPARD].stock,
          info[u][RONA_MARTINGROVE].stock,
        ].join(",")
      );
    } catch (e) {
      console.log("Couldn't write: ", u, info[u]);
    }
  }
};

module.exports = {
  writeStockToCSV,
};
