const cheerio = require("cheerio");
const axios = require("axios");
const fs = require("fs");
const neatCsv = require("neat-csv");
let outputfile = "./product_data.csv";
(async () => {
  let linkFile = await fs.readFileSync("./hh_links.csv", "utf-8");
  let parsed = await neatCsv(linkFile);
  fs.appendFileSync(outputfile, "link,name,description,image,keywords,weight");
  // console.log("parsed: ", parsed);
  for (let l of parsed) {
    try {
      let url = l.link; //`https://www.homehardware.ca/en/p/1034320`;

      console.log("about ot get URL: ", url);
      let response = await axios.get(url);
      let data = null;
      let rows = [];

      let $ = await cheerio.load(response.data);
      $("script").each((idx, elem) => {
        let elid = elem.attribs["id"];
        if (elid === "data-mz-preload-product") {
          data = elem.children[0].data;
          // console.log("Elem Data: ", elem.children[0].data);
        }
      });

      if (data) {
        let p = JSON.parse(data);

        let weight = p.measurements.packageWeight.value;
        let description = p.content.productFullDescription;
        if (description === undefined) {
          description = p.content.metaDescription;
        }
        if (description === undefined) {
          description = p.metaDescription;
        }
        if (description !== undefined) {
          description = `"${description
            .replace(/(<li>)/gi, "\n- ")
            .replace(/(<([^>]+)>)/gi, "")
            .replace(/"/g, '""')}"`;
        } else {
          description = "";
        }
        // console.log("p.metaDescription;: ", p);
        let title = p.content.productName ? p.content.productName : p.metaTitle;
        let keywords = p.content.metaTagKeywords
          ? p.content.metaTagKeywords
          : p.metaKeywords;
        fs.appendFileSync(outputfile, "\n");
        fs.appendFileSync(
          outputfile,
          [
            url,
            `"${title.replace(/"/g, '""')}"`,
            description,
            "https:" + p.mainImage.src,
            `"${keywords.toLowerCase().replace(/"/g, '""')}"`,
            weight,
          ].join(",")
        );
      }

      console.log("done");
    } catch (e) {
      console.log("error: with URL", e);
    }
  }
})();
