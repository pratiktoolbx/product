function sleep(milliseconds) {
  const date = Date.now();
  let currentDate = null;
  do {
    currentDate = Date.now();
  } while (currentDate - date < milliseconds);
}

const escapeXpathString = (str) => {
  const splitedQuotes = str.replace(/'/g, `', "'", '`);
  return `concat('${splitedQuotes}', '')`;
};

const clickByText = async (page, text) => {
  const escapedText = escapeXpathString(text);
  const linkHandlers = await page.$x(
    `//button[contains(text(), ${escapedText})]`
  );

  if (linkHandlers.length > 0) {
    // console.log("Link: ", linkHandlers[0]);
    await linkHandlers[0].click();
  } else {
    throw new Error(`Link not found: ${text}`);
  }
};

const changeStore = async (page, postalCode) => {
  await Promise.all([
    page.goto("https://www.lowes.ca/stores"),
    // page.waitForNavigation({ waitUntil: "networkidle0" }),
  ]);
  sleep(2000);
  console.log("10");
  let addressElement = await page.$(".titleMainWrapper");
  let addressText = await page.evaluate(
    (addressElement) => addressElement.textContent,
    addressElement
  );

  console.log("Address Text: ", addressText);
  if (addressText.indexOf(postalCode) > -1) {
    console.log("already at store", addressText.replace(/^\s+/g, ""));
    return;
  }

  console.log("Adding postal: ", postalCode);
  await page.type("input[name='search-term']", postalCode, { delay: 20 });
  await page.keyboard.press(String.fromCharCode(13));
  sleep(1000);

  let newStoreListLoaded = false;
  while (!newStoreListLoaded) {
    sleep(1000);

    newStoreListLoaded = true;
    await page
      .waitForSelector(".formSubmitted")
      .catch((e) => (newStoreListLoaded = false));
  }
  //   console.log("Form Submitted");

  //   console.log("button find and press");
  await Promise.all([
    clickByText(page, "MAKE THIS MY STORE"),
    // page.waitForNavigation({ waitUntil: "networkidle0" }),
  ]);
  addressElement = await page.$(".titleMainWrapper");
  addressText = await page.evaluate(
    (addressElement) => addressElement.textContent,
    addressElement
  );

  console.log("Address Text: ", addressText);
};

module.exports = {
  changeStore,
};
