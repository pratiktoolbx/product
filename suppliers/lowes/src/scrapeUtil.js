const cheerio = require("cheerio");

const getSpecification = ($, specification) => {
  var category = $("td")
    .filter(function () {
      return $(this).text().trim().indexOf(specification) > -1;
    })
    .next()
    .text()
    .replace(/\n/g, "")
    .replace(/^\s+/g, "")
    .replace("'", "");
  return category;
};

const getProductDetailsFromPage = async (content) => {
  let $ = await cheerio.load(content);

  let productName = $("h1.product-name").text();
  let itemNumber = $("analytics[data-analytics-id='product-id']").text();
  let imageUrl = $(".product-image > img").attr("src");
  let price = parseFloat($(".price-actual").text().replace("$", ""));
  const descriptions = [];
  $(".product-overview > ul > li").each(function (i, elem) {
    descriptions[i] = $(this).text();
  });
  let description = descriptions.join(". ") + ".";

  // Specifications

  let species = getSpecification($, "Wood Species");
  let weight = getSpecification($, "Weight");
  let length = getSpecification($, "Actual Length (ft)");

  //   console.log("ITEM: ", item);
  return {
    productName,
    itemNumber,
    imageUrl,
    price,
    description,
    species,
    weight,
    length,
  };
};

module.exports = {
  getProductDetailsFromPage,
};
