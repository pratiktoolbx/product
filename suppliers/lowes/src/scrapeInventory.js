/*
 * Scrape Item Inventory
 * Takes product URLs from ./output/products.csv
 * Scrapes item level information (stock and price).
 * It will iterate over a finite set of stores
 * It will store all inventory data per in a CSV ./output/item.csv
 * Stock: Currently formatted as URL, stockStoreA, stockStoreB, stockStoreC
 * Price: Currently formatted as URL, priceStoreA, priceStoreB, priceStoreC
 * Outputs info at ./output/stock.csv and ./output/prices.csv
 */

const cheerio = require("cheerio");
const puppeteer = require("puppeteer-extra");
const output = require("./output");
const csv = require("neat-csv");
const fs = require("fs");
const cs = require("./changeStore");
const { performance } = require("perf_hooks");

const escapeXpathString = (str) => {
  const splitedQuotes = str.replace(/'/g, `', "'", '`);
  return `concat('${splitedQuotes}', '')`;
};

const clickByText = async (page, text) => {
  const escapedText = escapeXpathString(text);
  const linkHandlers = await page.$x(
    `//button[contains(text(), ${escapedText})]`
  );

  if (linkHandlers.length > 0) {
    // console.log("Link: ", linkHandlers[0]);
    await linkHandlers[0].click();
  } else {
    throw new Error(`Link not found: ${text}`);
  }
};

function sleep(milliseconds) {
  const date = Date.now();
  let currentDate = null;
  do {
    currentDate = Date.now();
  } while (currentDate - date < milliseconds);
}

const getInventoryData = async (page, productUrl) => {
  try {
    console.log("Starting: ", productUrl);

    await Promise.all([
      page.goto(productUrl),
      page.waitForNavigation({ waitUntil: "networkidle0" }),
    ]);

    let div_selector_to_remove = ".modal-sign-up";
    if ((await page.$(div_selector_to_remove)) !== null) {
      console.log("modal sign up found");
      await page.evaluate((sel) => {
        document.querySelector(sel).parentElement.remove();
      }, div_selector_to_remove);
    } else {
      console.log("modal sign up not found");
    }

    await Promise.all([
      clickByText(page, "Check Availability"),
      page
        .waitForSelector(".modal-dialog")
        .catch((e) => (newStoreListLoaded = false)),
      page
        .waitForSelector(".modal-body > .results")
        .catch((e) => (newStoreListLoaded = false)),
    ]);

    let content = await page.content();
    let $ = cheerio.load(content);

    let p = {};
    p.productUrl = productUrl;

    $(".list-group-item").map((i, el) => {
      let inventory = $(el).find(".count-in-store").text();
      let store = $(el).find(".store-title").text();
      p[store] = inventory;
      //   console.log("Row: ", store, inventory);
      //   return { store, inventory };
    });
    return p;

    // console.log("Product: ", p);

    // let inventory = $(".count-in-store").text();
    // let store = $(".store-title").text();
    // console.log("INventory: ", store, inventory);
  } catch (e) {
    console.log(`Couldn't get product info`, e);
  }
  return {};
};

(async () => {
  const t0 = performance.now();

  const browser = await puppeteer.launch({
    headless: true,
  });

  const page = await browser.newPage();

  let postals = {
    castlefield: "M6B 4B3",
  };

  let count = 0;

  for (const [supplierStore, postalCode] of Object.entries(postals)) {
    console.log(`${supplierStore}: ${postalCode}`);

    let products = await csv(fs.readFileSync("./output/products.csv"));

    await cs.changeStore(page, postalCode);

    output.startInventoryCSV();
    for (let p of products) {
      let inventory = await getInventoryData(page, p.product_url);
      if (
        (inventory.productUrl !== "" || inventory.productUrl !== undefined) &&
        (inventory["TORONTO - YORK"] !== undefined ||
          inventory["TORONTO - YORK"] !== "")
      ) {
        output.writeInventoryToCSV(inventory);
      }

      sleep(Math.floor(Math.random() * (2000 - 500)) + 1000);
    }
  }

  const t1 = performance.now();
  console.log("Scraping took " + (t1 - t0) / 1000 + " seconds.");

  browser.close();
})();
