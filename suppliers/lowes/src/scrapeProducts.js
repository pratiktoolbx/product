/*
 * Scrape Products
 * Takes product URLs from ./output/product-list.csv
 * Scrapes product level information (everything but inventory).
 * The scraper anchors to a single store.
 * If there are product variants on page, the scraper will recursively
 * iterate over each variant and store it as a separate product.
 * The scraper saves all information to
 * ./output/products.csv. The lowes scraper currently doesn't include weight.
 */

const cheerio = require("cheerio");
const puppeteer = require("puppeteer-extra");
const output = require("./output");
const csv = require("neat-csv");
const fs = require("fs");

const { performance } = require("perf_hooks");

const getProductDetailObject = async (page) => {
  // get react components off page
  let reactComps = await page.evaluate(() => components);

  let productDetails = null;
  for (let c of reactComps) {
    if (c.componentName === "ProductDetail") {
      productDetails = c.props.data;
    }
  }

  return productDetails;
};

const getSpecification = ($, specification) => {
  var category = $("td")
    .filter(function () {
      return $(this).text().trim().indexOf(specification) > -1;
    })
    .next()
    .text()
    .replace(/\n/g, "")
    .replace(/^\s+/g, "")
    .replace("'", "");
  return category;
};
const getProductDetailsFromPage = async (content) => {
  let $ = await cheerio.load(content);

  let productName = $("h1.product-name").text();
  let itemNumber = $("analytics[data-analytics-id='product-id']").text();
  let imageUrl = $(".product-image > img").attr("src");
  let price = parseFloat($(".price-actual").text().replace("$", ""));
  const descriptions = [];
  $(".product-overview > ul > li").each(function (i, elem) {
    descriptions[i] = $(this).text();
  });
  let description = descriptions.join(". ") + ".";

  // Specifications

  let species = getSpecification($, "Wood Species");
  let weight = getSpecification($, "Weight");
  let length = getSpecification($, "Actual Length (ft)");

  //   console.log("ITEM: ", item);
  return {
    productName,
    itemNumber,
    imageUrl,
    price,
    description,
    species,
    weight,
    length,
  };
};

const processVariants = async (page) => {
  let productDetail = await getProductDetailObject(page);

  //   console.log("productDetail.product:", productDetail.product);
  let variantMatrix = productDetail.product.productVariants.variantMatrix;

  let variantUrls = [];
  for (let v of variantMatrix) {
    for (let vItems of v) {
      if (vItems.key === "Variant") {
        let url = vItems.value.url;
        variantUrls.push("https://www.lowes.ca" + url);
      }
    }
  }

  return variantUrls;
};

const getInventoryData = async (page, sku) => {
  let content = await page.evaluate(async (sku) => {
    let inventoryUrl = `https://www.lowes.ca/api/store/GetStoreInventories?sku=${sku}&storeIds=3096OL,3162OL,2640OL,2631OL,3097OL,2493OL,3321OL,2633OL,2632OL,3327OL,3062OL,2494OL,2413OL,3201OL,2642OL,2971OL,2741OL,2888OL,3224OL,3092OL,2492OL,3710OL,2934OL,2889OL`;
    const response = await fetch(inventoryUrl);
    const text = await response.text();
    return text;
  });

  console.log("content: ", content);
  let inventory = JSON.parse(content);
  return inventory;
};
const getMasterProductInformation = async (
  page,
  productUrl,
  departmentUrl,
  hasVariants
) => {
  try {
    console.log("Starting: ", productUrl);

    await Promise.all([
      page.goto(productUrl),
      page.waitForNavigation({ waitUntil: "networkidle0" }),
    ]);

    if (hasVariants == true || hasVariants === "true") {
      // recursively check variants and pull info
      //   console.log("hasVariants: ", hasVariants);
      let variantUrls = await processVariants(page);
      for (let i = 0; i < variantUrls.length; i++) {
        await getMasterProductInformation(
          page,
          variantUrls[i],
          departmentUrl,
          false
        );
      }
    } else {
      let content = await page.content();
      let item = await getProductDetailsFromPage(content);
      //   let inventory = await getInventoryData(page, item.itemNumber);
      output.writeProductLineToCSV({
        productUrl: productUrl,
        itemName: item.productName,
        itemNumber: item.itemNumber,
        price: item.price,
        departmentUrl: departmentUrl,
        description: item.description,
        weight: item.weight,
        image: item.imageUrl,
      });
    }
  } catch (e) {
    console.log(`Couldn't get produc tnifo`, e);
  }
};

(async () => {
  const t0 = performance.now();

  const browser = await puppeteer.launch({
    headless: true,
  });

  const page = await browser.newPage();

  let postals = {
    castlefield: "M6B 4B3",
  };

  let count = 0;

  for (const [supplierStore, postalCode] of Object.entries(postals)) {
    console.log(`${supplierStore}: ${postalCode}`);

    let products = await csv(fs.readFileSync("./output/productlist.csv"));

    output.startProductCSV();
    for (let p of products) {
      await getMasterProductInformation(
        page,
        p.productUrl,
        p.departmentUrl,
        p.hasVariants
      );
    }
  }

  const t1 = performance.now();
  console.log("Scraping took " + (t1 - t0) / 1000 + " seconds.");

  browser.close();
})();
