const postgres = require("postgres");
//https://www.npmjs.com/package/postgres

// const sql = postgres(
//   "postgres://toolbx_master:MFXB;n23qmth{Rw8aAZ{Q}kp3xS&drMX+eRpR2YhxN3$;4j,UOrguP554xLX^rsG@production-db.c9sgfrlcftod.us-east-1.rds.amazonaws.com/toolbx",
//   {}
// );

const sql = postgres(
  "postgres://toolbxmaster:BCTq[Go)|6L7}yK.xuC9yF(NVUU%26;Id8l=ZKf1aGKQ2do89Pv3UnLGtZAqStYe7@staging-db.c9sgfrlcftod.us-east-1.rds.amazonaws.com/toolbx",
  {}
);

function escapeString(str) {
  return str.replace(/[\0\x08\x09\x1a\n\r"'\\\%]/g, function (char) {
    switch (char) {
      case "\0":
        return "\\0";
      case "\x08":
        return "\\b";
      case "\x09":
        return "\\t";
      case "\x1a":
        return "\\z";
      case "\n":
        return "\\n";
      case "\r":
        return "\\r";
      case '"':
      case "'":
      case "\\":
      case "%":
        return "\\" + char; // prepends a backslash to backslash, percent,
      // and double/single quotes
      default:
        return char;
    }
  });
}

// const psql = require("../util/postgres");
// const sql = psql.sql;

const getCurrentProductList = async () => {
  try {
    console.log("Getting Current Product List From Select...");
    const products = {};
    let records = await sql`SELECT distinct("p"."id"),"p"."name","p"."price","p"."sku","p"."isActive" from "supplier" "s" join "category" "c" on "c"."supplierId" = "s"."id" join "product_categories_category" "pcc" on "c"."id"="pcc"."categoryId" join "product" "p" on "pcc"."productId"="p"."id" where "s"."id" in ('70f27e1f-38e3-4797-a195-3236e7b0a56e')`;
    // let records = await sql`SELECT distinct("p"."id"),"p"."name","p"."price","p"."sku","p"."isActive" from "supplier" "s" join "category" "c" on "c"."supplierId" = "s"."id" join "product_categories_category" "pcc" on "c"."id"="pcc"."categoryId" join "product" "p" on "pcc"."productId"="p"."id" where "s"."id" in ('c79b3940-0e60-4025-b996-559d6fa2e8b6') and "p"."id"='0fa474c6-d116-4d68-a73f-4f5ddd4a802d'`;

    if (records.count > 0) {
      for (let r of records) {
        products[r.id] = {
          name: r.name,
          current: r.price,
          sku: r.sku,
          isActive: r.isActive,
        };
      }
    } else {
      console.log("No records found");
    }
    return products;
  } catch (e) {
    console.log("Couldn't get current products: ", e);
    return {};
  }
};

module.exports = {
  getCurrentProductList,
};
