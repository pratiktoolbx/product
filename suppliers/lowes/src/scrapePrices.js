/*
 * Scrape Departments
 * Takes in URLs of specific department pages and pulls
 * product URLs. It saves all information to
 * ./output/product-list.csv. It will at minimum include the
 * department URL and product URL. It is a prerequisite for
 * scrapeProducts.js
 */

const cheerio = require("cheerio");
const puppeteer = require("puppeteer-extra");
const moment = require("moment");
const output = require("./output");
const csv = require("neat-csv");
const fs = require("fs");
const { GoogleSpreadsheet } = require("google-spreadsheet");
const { performance } = require("perf_hooks");
const db = require("./db");
const scrapeUtil = require("./scrapeUtil");
const creds = require("../google-cred.json");
const pullProductsFromList = async (content, departmentUrl) => {
  //   let response = await axios.get(url);
  let $ = await cheerio.load(content);

  let products = [];

  //   console.log("products listt: ", products);
  $("ul.wishlist.wishlist-noactions li").each(function (i, elm) {
    // console.log($(this).text()); // for testing do text()
    let url = $(this)
      .find(".card > .product-content-top > .content-image > a")
      .attr("href");

    if (url && url != undefined && url.length > 0) {
      products.push({
        departmentUrl,
        productUrl: "https://www.lowes.ca" + url,
        itemNumber: $(this)
          .find(".card > .product-content-top > .card-block")
          .find("span[data-analytics-id='sku']")
          .text(),
        itemName: $(this)
          .find(".card > .product-content-top > .card-block > .link-black")
          .text(),
        priceRange: $(this)
          .find(".card > .product-content-top > .card-block > .price")
          .text(),
        hasVariants: $(this)
          .find(".card > .product-content-top > .card-block > .price")
          .text()
          .match(/\sto\s/)
          ? true
          : false,
      });
    }
  });

  //   console.log("Products: ", products);
  return products;
};

(async () => {
  console.log("starting: ");
  const t0 = performance.now();

  const browser = await puppeteer.launch({
    headless: true,
  });

  const page = await browser.newPage();

  let postals = {
    castlefield: "M6B 4B3",
  };

  let count = 0;

  const productsScraped = [];
  for (const [supplierStore, postalCode] of Object.entries(postals)) {
    console.log(`${supplierStore}: ${postalCode}`);

    // await changeStore(page, postalCode);

    let departments = await csv(fs.readFileSync("./departments2.csv"));

    // output.startProductListCSV();
    // console.log("departments : ", departments);

    // get prices from department pages as much as possible.
    for (let d of departments) {
      let url = d.link;
      try {
        count = count + 1;

        await Promise.all([
          page.goto(url, { timeout: 60000 }),
          page.waitForNavigation({ waitUntil: "networkidle0" }),
        ]);
        console.log("Parsing URL: ", url);
        let content = await page.content();

        let products = await pullProductsFromList(content, url);
        productsScraped.push(...products);
        // output.writeProductListToCSV(products);
        // console.log(`Found ${products.length} products at URL: ${url} `);
      } catch (e) {
        console.log(`Couldn't get url ${url}`, e);
      }
    }

    console.log("Products Scraped By Departmeent: ", productsScraped.length);
    // get current products
    // let currentProducts = await db.getCurrentProductList();
    let prevScrapedProducts = await csv(
      fs.readFileSync("./output/products.csv")
    );
    let currentProducts = [...prevScrapedProducts];

    console.log("Current Products Lngth: ", currentProducts.length);
    console.log("Prev Products Lngth: ", prevScrapedProducts.length);
    console.log("dep Products Lngth: ", productsScraped.length);
    // console.log("Current Products Lngth: ", currentProducts.length);
    let remainingProducts = [];
    for (let p of currentProducts) {
      //   console.log("starting product: ", p);
      // add sourceUrl to all products
      for (let sp of prevScrapedProducts) {
        if (sp.item_number === p.item_number) {
          p["sku"] = p.item_number; // just because we are pulling from a local file
          p["sourceUrl"] = sp["product_url"];
          break;
        }
      }

      p["found"] = false;
      //   console.log("P: ", p);
      for (let depScraped of productsScraped) {
        // console.log("Department SCrape: ", depScraped);
        if (
          depScraped.itemNumber === p.sku &&
          depScraped.hasVariants === false
        ) {
          // console.log("Department SCrape: ", depScraped);
          p["newPrice"] = parseFloat(depScraped["priceRange"].replace("$", ""));
          p["diff"] = p.newPrice / parseFloat(p.price);
          p["found"] = true;
          console.log("Priice updated");
          break;
        }
      }

      if (!p.found) {
        remainingProducts.push(p);
      }
    }

    // // loop through remaining products and get price directly from page
    for (let rp of remainingProducts) {
      if (rp.sourceUrl !== undefined) {
        await Promise.all([
          page.goto(rp.sourceUrl),
          page.waitForNavigation({ waitUntil: "networkidle0" }),
        ]);
        let content = await page.content();
        let details = await scrapeUtil.getProductDetailsFromPage(content);
        console.log("Details: ", details);
        for (let p of currentProducts) {
          if (p.sku === rp.sku) {
            p["newPrice"] = parseFloat(details.price);
            p["diff"] = p.newPrice / parseFloat(p.price);
            p["found"] = true;
          }
        }
      }
    }

    fs.writeFileSync(
      "./output/prices.csv",
      "P_ID,SKU,NAME,CURRENT_PRICE,NEW_PRICE,DIFF\n"
    );
    for (let p of currentProducts) {
      if (p.newPrice !== undefined) {
        fs.appendFileSync(
          "./output/prices.csv",
          `${"PROD ID HERE"},${p.sku},${p.name}.${p.price},${p.newPrice},${
            p.diff
          }\n`
        );
      }
    }

    // spreadsheet key is the long id in the sheets URL
    const doc = new GoogleSpreadsheet(
      "1dfR6yXvHuhNI5urr2EkcJAAbz0ZdGgue0Qm9Uiu6mLw"
    );

    // use service account creds
    await doc.useServiceAccountAuth(creds);
    await doc.loadInfo();
    let priceSheet = doc.sheetsById["0"];

    await priceSheet.clear();
    await priceSheet.setHeaderRow(
      "P_ID,SKU,NAME,CURRENT_PRICE,NEW_PRICE,DIFF".split(",")
    );
    let rows = [];
    for (let p of currentProducts) {
      if (p.newPrice !== undefined) {
        rows.push({
          P_ID: null,
          SKU: p.sku,
          NAME: p.name,
          CURRENT_PRICE: p.price,
          NEW_PRICE: p.newPrice,
          DIFF: p.diff,
        });
      }
    }
    await priceSheet.addRows(rows);

    const summarySheet = doc.sheetsById["644794416"];
    const summaryRows = await summarySheet.getRows();
    console.log(summaryRows[0].task);
    summaryRows[0].date = moment().format("YYYY-MM-DD");
    await summaryRows[0].save();
    // console.log("Price SHeet: ", priceSheet.title);
    // priceSheet.setHeaderRow;
  }

  const t1 = performance.now();
  console.log("Scraping took " + (t1 - t0) / 1000 + " seconds.");

  browser.close();
})();
