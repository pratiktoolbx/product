const fs = require("fs");

const RONA_STOCKYARDS = "stockyards";
const RONA_SHEPPARD = "sheppard";
const RONA_MARTINGROVE = "martingrove";

const startInventoryCSV = () => {
  let filename = "./output/inventory.csv";
  fs.writeFileSync(filename, "");
  fs.appendFileSync(
    filename,
    `productUrl,TORONTO - YORK,ETOBICOKE - SOUTH,VAUGHAN,TORONTO - DANFORTH,SCARBOROUGH - WEST,MAPLE,MISSISSAUGA,BRAMPTON - SOUTH,SCARBOROUGH - NORTH / MARKHAM,BRAMPTON - NORTH,PICKERING,MILTON,NEWMARKET - EAST GWILLIMBURY,BURLINGTON,WHITBY,OSHAWA,HAMILTON,ANCASTER,NIAGARA FALLS,BARRIE,CAMBRIDGE,BRANTFORD,KITCHENER,WATERLOO`
  );
};

const writeInventoryToCSV = (p) => {
  // Update File
  let filename = "./output/inventory.csv";

  try {
    if (p.productUrl === undefined || p.productUrl === "") return;
    fs.appendFileSync(filename, "\n");

    fs.appendFileSync(
      filename,
      [
        p.productUrl,
        p["TORONTO - YORK"],
        p["ETOBICOKE - SOUTH"],
        p["VAUGHAN"],
        p["TORONTO - DANFORTH"],
        p["SCARBOROUGH - WEST"],
        p["MAPLE"],
        p["MISSISSAUGA"],
        p["BRAMPTON - SOUTH"],
        p["SCARBOROUGH - NORTH / MARKHAM"],
        p["BRAMPTON - NORTH"],
        p["PICKERING"],
        p["MILTON"],
        p["NEWMARKET - EAST GWILLIMBURY"],
        p["BURLINGTON"],
        p["WHITBY"],
        p["OSHAWA"],
        p["HAMILTON"],
        p["ANCASTER"],
        p["NIAGARA FALLS"],
        p["BARRIE"],
        p["CAMBRIDGE"],
        p["BRANTFORD"],
        p["KITCHENER"],
        p["WATERLOO"],
      ].join(",")
    );
  } catch (e) {
    console.log("Couldn't write: ", p, e);
  }
};

const startProductListCSV = () => {
  let filename = "./output/productlist.csv";
  fs.writeFileSync(filename, "");
  fs.appendFileSync(
    filename,
    `departmentUrl,productUrl,itemNumber,name,priceRange,hasVariants`
  );
};
const writeProductListToCSV = (products) => {
  // Update File
  let filename = "./output/productlist.csv";

  for (let p of products) {
    // console.log(info[u]);
    try {
      fs.appendFileSync(filename, "\n");

      fs.appendFileSync(
        filename,
        [
          p.departmentUrl,
          p.productUrl,
          `"${p.itemNumber.replace(/"/g, '""')}"`,
          `"${p.itemName.replace(/"/g, '""')}"`,
          `"${p.priceRange.replace(/"/g, '""')}"`,
          p.hasVariants,
        ].join(",")
      );
    } catch (e) {
      console.log("Couldn't write: ", p, e);
    }
  }
};

const startProductCSV = () => {
  let filename = "./output/products.csv";

  fs.writeFileSync(filename, "");
  fs.appendFileSync(
    filename,
    `product_url,name,item_number,price, department_url,description,weight,imagees`
  );
};
const writeProductLineToCSV = (p) => {
  let filename = "./output/products.csv";

  try {
    fs.appendFileSync(filename, "\n");

    fs.appendFileSync(
      filename,
      [
        p.productUrl,
        `"${p.itemName.replace(/"/g, '""')}"`,
        p.itemNumber,
        p.price,
        p.departmentUrl,
        `"${p.description.replace(/"/g, '""')}"`,
        p.weight,
        p.image,
      ].join(",")
    );
  } catch (e) {
    console.log("Couldn't write: ", p, e);
  }
};
const writeStockToCSV = (info) => {
  // Update File
  let filename = "./output/products.csv";
  fs.writeFileSync(filename, "");
  fs.appendFileSync(
    filename,
    `product_url,name,item_number,price, department_url,description,weight,imagees`
  );
  for (let p of products) {
    // console.log(info[u]);
    try {
      fs.appendFileSync(filename, "\n");

      fs.appendFileSync(
        filename,
        [
          p.productUrl,
          `${p.itemName.replace(/"/g, '""')}`,
          p.itemNumber,
          p.price,
          p.departmentUrl,
          `${p.description.replace(/"/g, '""')}`,
          p.weight,
          p.image,
        ].join(",")
      );
    } catch (e) {
      console.log("Couldn't write: ", p, e);
    }
  }
};

module.exports = {
  writeProductListToCSV,
  startProductListCSV,
  writeStockToCSV,
  startProductCSV,
  writeProductLineToCSV,
  startInventoryCSV,
  writeInventoryToCSV,
};
