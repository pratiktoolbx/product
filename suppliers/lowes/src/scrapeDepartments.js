/*
 * Scrape Departments
 * Takes in URLs of specific department pages and pulls
 * product URLs. It saves all information to
 * ./output/product-list.csv. It will at minimum include the
 * department URL and product URL. It is a prerequisite for
 * scrapeProducts.js
 */

const cheerio = require("cheerio");
const puppeteer = require("puppeteer-extra");
const output = require("./output");
const csv = require("neat-csv");
const fs = require("fs");
const { performance } = require("perf_hooks");

const pullProductsFromList = async (content, departmentUrl) => {
  //   let response = await axios.get(url);
  let $ = await cheerio.load(content);

  let products = [];

  //   console.log("products listt: ", products);
  $("ul.wishlist.wishlist-noactions li").each(function (i, elm) {
    // console.log($(this).text()); // for testing do text()
    let url = $(this)
      .find(".card > .product-content-top > .content-image > a")
      .attr("href");

    if (url && url != undefined && url.length > 0) {
      products.push({
        departmentUrl,
        productUrl: "https://www.lowes.ca" + url,
        itemNumber: $(this)
          .find(".card > .product-content-top > .card-block")
          .find("span[data-analytics-id='sku']")
          .text(),
        itemName: $(this)
          .find(".card > .product-content-top > .card-block > .link-black")
          .text(),
        priceRange: $(this)
          .find(".card > .product-content-top > .card-block > .price")
          .text(),
        hasVariants: $(this)
          .find(".card > .product-content-top > .card-block > .price")
          .text()
          .match(/\sto\s/)
          ? true
          : false,
      });
    }
  });

  //   console.log("Products: ", products);
  return products;
};

const getCSVFile = () => {};

(async () => {
  console.log("starting: ");
  const t0 = performance.now();

  const browser = await puppeteer.launch({
    headless: true,
  });

  const page = await browser.newPage();

  let postals = {
    castlefield: "M6B 4B3",
  };

  let count = 0;

  for (const [supplierStore, postalCode] of Object.entries(postals)) {
    console.log(`${supplierStore}: ${postalCode}`);

    // await changeStore(page, postalCode);

    let departments = await csv(fs.readFileSync("./departments2.csv"));
    let urls = [
      "https://www.lowes.ca/dept/dimensional-lumber-lumber-building-supplies-a541?display=1000",
    ];

    output.startProductListCSV();
    // console.log("departments : ", departments);
    for (let d of departments) {
      let url = d.link;
      try {
        count = count + 1;

        await Promise.all([
          page.goto(url, { timeout: 60000 }),
          page.waitForNavigation({ waitUntil: "networkidle0" }),
        ]);
        console.log("Parsing URL: ", url);
        let content = await page.content();

        let products = await pullProductsFromList(content, url);
        output.writeProductListToCSV(products);
        console.log(`Found ${products.length} products at URL: ${url} `);
      } catch (e) {
        console.log(`Couldn't get url ${url}`, e);
      }
    }
  }

  const t1 = performance.now();
  console.log("Scraping took " + (t1 - t0) / 1000 + " seconds.");

  browser.close();
})();
