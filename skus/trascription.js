const psql = require("./src/util/postgres");
const sql = psql.sql;
require("dotenv").config();

const fs = require("fs");
const csv = require("csv-parser");

const loadTop = async () => {
  let results = [];
  await new Promise((resolve, reject) => {
    fs.createReadStream("./csv/transcription.csv")
      .pipe(csv())
      .on("data", (data) => results.push(data))
      .on("end", () => {
        resolve(results);
      });
  });

  return results;
};

(async () => {
  try {
    let products = await loadTop();

    let totalProd = 0;
    let foundProd = 0;
    let foundSku = 0;

    for (let p of products) {
      if (p.Status !== "Good") continue;
      console.log(p.P_ID);

      let a = await sql`select id,name,sku from "product" where id=${p.P_ID}`;
      if (a.count > 0) {
        // console.log(a);
        foundProd += 1;
        if (a[0].sku != null) {
          foundSku += 1;
          //   console.log(a[0].id, a[0].name, a[0].sku);
        } else {
          // console.log(`update "product" set sku=${p.SKU} where id=${a[0].id}`);
          let u = await sql`update "product" set sku=${p.SKU} where id=${a[0].id}`;
          console.log("update: ", u);
        }
      } else {
        // console.log("Didn't find: ", p.name);
      }
      totalProd += 1;
    }
    await sql.end();
    console.log("TOtal prod: ", totalProd);
    console.log("Found prod: ", foundProd);
    console.log("Found SKU: ", foundSku);
  } catch (e) {
    console.log(e);
  }
})();
