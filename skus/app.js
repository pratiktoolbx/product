const sf = require("./src/util/salesforce");
const psql = require("./src/util/postgres");
const sql = psql.sql;
require("dotenv").config();

(async () => {
  try {
    await sf.connect();
    let orders = await sf.getOrderDetails(10);

    // console.log(orders);
    let a = await sql`
select name from product where id='3d37acf2-7cb4-4991-98e0-3f964edaf709'
`;
    console.log(a);

    for (let o of orders) {
      console.log("o.OrderNumber:", o.OrderNumber);
      let line = o.Material__c.split("\n");
      let material = null;
      let sku = null;
      let count = 0;
      for (let l of line) {
        let t = l.trim();
        // console.log(t);
        if (t.indexOf("1000") == 0 && t.length === 10) {
          sku = t;
        }
        let mat = t.match(/.+?(?= Qty)/);
        if (mat) {
          let arr = mat[0].split(" ");
          material = arr.splice(1, arr.length - 2).join(" ");
          // console.log(material);
        }

        count += 1;
        if (count % 2 === 0) {
          if (sku && material) {
            console.log(`Material: ${material}, SKU: ${sku}`);

            let a = await sql`
              select id,name,sku from product where name='${psql.escapeString(
                material
              )}'
            `;
            console.log(a);
          }
          sku = null;
          material = null;
        }

        // if (t.length > 0 && /Qty: \d+ SKU: 1000\d+/.test(t)) {
        // }
      }
    }
  } catch (e) {
    console.log(e);
  }
})();
