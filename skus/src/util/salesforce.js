const jsforce = require("jsforce");
require("dotenv").config();

let conn = new jsforce.Connection({
  oauth2: {
    clientId: process.env.SALESFORCE_CLIENT_ID,
    clientSecret: process.env.SALESFORCE_CLIENT_SECRET,
    loginUrl: process.env.SALESFORCE_LOGIN_URI,
    redirectUri: process.env.SALESFORCE_REDIRECT_URI,
  },
});
conn.bulk.pollTimeout = 60000;

const connect = async () => {
  try {
    await conn.login(
      process.env.SALESFORCE_USERNAME,
      process.env.SALESFORCE_PASSWORD
    );
    console.log("Logged In: ", conn.accessToken);
  } catch (e) {
    console.log("Exception when trying to login: ", e);
  }
  return null;
};

const getOrderDetails = async (days) => {
  try {
    let result = await conn.query(
      `SELECT 
      OrderNumber, 
      EffectiveDate,
      Pick_Up_Location__r.Name,
      Type,
      Status,
      Material__c,
      Description
      FROM 
        Order 
      WHERE 
        EffectiveDate > LAST_N_DAYS:${days}
        and Type = 'TOOLBX Select'
        and (Pick_Up_Location__r.Name = 'Home Depot - Leaside (7073)' or Pick_Up_Location__r.Name = 'Home Depot - Yorkdale (7129)')
      LIMIT 100`
    );

    // console.log("Order length: ", orders.records.length);
    return result.records;
  } catch (e) {
    console.error(e);
  }
  return null;
};
const getOrderItems = async (orderNumber) => {
  try {
    let result = await conn.query(
      `SELECT 
        Order.OrderNumber, 
        Order.EffectiveDate, 
        Order.Pickup_Location_Address__c, 
        Invoice_Product_Name__c, 
        Quantity, 
        App_Quote__c, 
        Markup_Percentage__c, 
        Markup_Unit_Price__c, 
        Total_Markup_Amount__c 
      FROM 
        OrderItem 
      WHERE 
        Order.OrderNumber='${orderNumber}'`
    );

    // console.log("Order length: ", orders.records.length);
    return result.records;
  } catch (e) {
    console.error(e);
  }
  return null;
};

module.exports = {
  connect,
  getOrderDetails,
  getOrderItems,
};
