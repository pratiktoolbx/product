const psql = require("./src/util/postgres");
const sql = psql.sql;
require("dotenv").config();

const fs = require("fs");
const csv = require("csv-parser");

const loadTop = async () => {
  let results = [];
  await new Promise((resolve, reject) => {
    fs.createReadStream("./csv/top.csv")
      .pipe(csv())
      .on("data", (data) => results.push(data))
      .on("end", () => {
        resolve(results);
      });
  });

  return results;
};

(async () => {
  try {
    let products = await loadTop();

    let totalProd = 0;
    let foundProd = 0;
    let foundSku = 0;
    // console.log(orders);
    let cols = ["id", "name"];
    for (let p of products) {
      //   console.log(p.name);
      let a = await sql`select id,name,sku from "product" where name like ${
        p.name + "%"
      }`;
      if (a.count > 0) {
        // console.log(a);
        foundProd += 1;
        if (a[0].sku != null) {
          foundSku += 1;
        } else {
          let u = await sql`update "product" set sku=${p.sku} where id=${a[0].id}`;
          console.log("update: ", u);
        }
      } else {
        // console.log("Didn't find: ", p.name);
      }
      totalProd += 1;
    }
    console.log("TOtal prod: ", totalProd);
    console.log("Found prod: ", foundProd);
    console.log("Found SKU: ", foundSku);
  } catch (e) {
    console.log(e);
  }
})();
