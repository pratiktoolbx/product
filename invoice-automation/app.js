// const cheerio = require('cheerio')
// const axios = require("axios");
const fs = require("fs").promises;
const salesforce = require("./utils/salesforce");
const pdf = require("./utils/pdf");
const model = require("./utils/model");

// TOOLBX Select
// const ORDER_NUMBER = "TB-25211"; //short
// const ORDER_NUMBER = "TB-24978"; // medium
// const ORDER_NUMBER = "TB-25526"; // long

// delivery only
// const ORDER_NUMBER = "TB-22270";

// site to site (supplier delivery)
// const ORDER_NUMBER = "TB-22590";

// site to site (supplier delivery - not invoiced yet (for packing slip))
const ORDER_NUMBER = "TB-26580";

// subscription
// const ORDER_NUMBER = "TB-22547";

(async () => {
  try {
    console.log("STARTING: ");

    await salesforce.connect();
    console.log("Connected");
    let order = await salesforce.getOrderDetails(ORDER_NUMBER);
    console.log("Got Order Details For Order: ", ORDER_NUMBER);
    let orderItems = await salesforce.getOrderItems(ORDER_NUMBER);
    console.log("Got Order Items For Order: ", ORDER_NUMBER);

    let invoice = model.generateModel(order, orderItems);

    pdf.createInvoice(invoice, `./pdfs/${ORDER_NUMBER}.pdf`);
    console.log("Generated PDF");
  } catch (e) {
    console.log("error: ", e);
  }
})();
