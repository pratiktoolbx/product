const jsforce = require("jsforce");
require("dotenv").config();

let conn = new jsforce.Connection({
  oauth2: {
    clientId: process.env.SALESFORCE_CLIENT_ID,
    clientSecret: process.env.SALESFORCE_CLIENT_SECRET,
    loginUrl: process.env.SALESFORCE_LOGIN_URI,
    redirectUri: process.env.SALESFORCE_REDIRECT_URI,
  },
});
conn.bulk.pollTimeout = 60000;

const connect = async () => {
  try {
    await conn.login(
      process.env.SALESFORCE_USERNAME,
      process.env.SALESFORCE_PASSWORD
    );
    console.log("Logged In: ", conn.accessToken);
  } catch (e) {
    console.log("Exception when trying to login: ", e);
  }
  return null;
};

const getOrderDetails = async (orderNumber) => {
  try {
    let result = await conn.query(
      `SELECT 
        OrderNumber, 
        EffectiveDate, 
        Type,
        Status,
        Material__c,
        Description,
        Pick_Up_Location__r.Name,
        Pick_Up_Location__r.Address__c,
        Pick_Up_Location__r.City__c,
        Pick_Up_Location__r.Province__c,
        Pick_Up_Location__r.Country__c,
        Pick_Up_Location__r.Postal_Code__c,
        PoNumber,
        Account_Name_Formula__c,
        Ordered_By_Name_Hidden__c,
        Drop_Off_Site_Contact_Name__c,
        Drop_Off_Site_Contact_Phone_Number__c,
        Delivery_Details__c,
        Drop_Off_Location__r.Name,
        Drop_Off_Location__r.Address__c,
        Drop_Off_Location__r.City__c,
        Drop_Off_Location__r.Province__c,
        Drop_Off_Location__r.Country__c,
        Drop_Off_Location__r.Postal_Code__c
      FROM 
        Order 
      WHERE 
        OrderNumber='${orderNumber}'
      LIMIT 1`
    );

    // console.log("Order length: ", orders.records.length);
    return result.records[0];
  } catch (e) {
    console.error(e);
  }
  return null;
};
const getOrderItems = async (orderNumber) => {
  try {
    let result = await conn.query(
      `SELECT 
        Order.OrderNumber, 
        Order.EffectiveDate, 
        Order.Pickup_Location_Address__c, 
        Invoice_Product_Name__c, 
        Quantity, 
        App_Quote__c, 
        Markup_Percentage__c, 
        Markup_Unit_Price__c, 
        Total_Markup_Amount__c 
      FROM 
        OrderItem 
      WHERE 
        Order.OrderNumber='${orderNumber}'`
    );

    // console.log("Order length: ", orders.records.length);
    return result.records;
  } catch (e) {
    console.error(e);
  }
  return null;
};

module.exports = {
  connect,
  getOrderDetails,
  getOrderItems,
};
