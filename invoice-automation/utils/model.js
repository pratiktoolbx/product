const generateModel = (order, orderItems) => {
  let invoice = {};

  invoice["items"] = [];
  invoice["type"] = order.Type;
  invoice["invoice_nr"] = order.OrderNumber;
  invoice["account"] = order.Account_Name_Formula__c;
  invoice["orderedBy"] = order.Ordered_By_Name_Hidden__c;
  invoice["effectiveDate"] = order.EffectiveDate;
  invoice["materialList"] = order.Material__c || order.Description;
  invoice["orderStatus"] = order.Status;

  invoice["dropoffName"] = order.Drop_Off_Site_Contact_Name__c;
  invoice["dropoffPhoneNumber"] = order.Drop_Off_Site_Contact_Phone_Number__c;
  invoice["dropoffNotes"] = order.Delivery_Details__c;
  invoice["effectiveDate"] = order.EffectiveDate;

  if (order.Type !== "Subscription") {
    invoice["pickupAddressLocation"] = order.Pickup_Location_Address__c;
    invoice["pickupAddressName"] = order.Pick_Up_Location__r.Name;
    invoice["poNumber"] = order.PoNumber;

    console.log("Pickup: ", order.Pick_Up_Location__r.Name);

    let ref;
    if (order.Type === "Site to Site") {
      ref = "to";
      invoice["customer"] = {
        name: order.Pick_Up_Location__r.Name,
        address: order.Pick_Up_Location__r.Address__c,
        city: order.Pick_Up_Location__r.City__c,
        state: order.Pick_Up_Location__r.Province__c,
        country: order.Pick_Up_Location__r.Country__c,
        postal_code: order.Pick_Up_Location__r.Postal_Code__c,
      };
      invoice["to"] = {
        name: order.Drop_Off_Location__r.Name,
        address: order.Drop_Off_Location__r.Address__c,
        city: order.Drop_Off_Location__r.City__c,
        state: order.Drop_Off_Location__r.Province__c,
        country: order.Drop_Off_Location__r.Country__c,
        postal_code: order.Drop_Off_Location__r.Postal_Code__c,
      };
    } else {
      invoice["customer"] = {
        name: order.Drop_Off_Location__r.Name,
        address: order.Drop_Off_Location__r.Address__c,
        city: order.Drop_Off_Location__r.City__c,
        state: order.Drop_Off_Location__r.Province__c,
        country: order.Drop_Off_Location__r.Country__c,
        postal_code: order.Drop_Off_Location__r.Postal_Code__c,
      };
    }
  }

  let subTotal = 0;
  for (let o of orderItems) {
    let qty = parseInt(o.Quantity);
    let unitPrice = o.Markup_Unit_Price__c;

    // console.log("Adding: ", o.Invoice_Product_Name__c, unitPrice.toFixed(2));
    invoice.items.push({
      product: o.Invoice_Product_Name__c,
      quantity: qty,
      unitPrice: unitPrice.toFixed(2),
      totalPrice: o.Total_Markup_Amount__c.toFixed(2),
    });

    subTotal += qty * unitPrice;
  }

  invoice["subTotal"] = subTotal.toFixed(2);
  invoice["tax"] = (subTotal * 0.13).toFixed(2);
  invoice["total"] = (subTotal * 1.13).toFixed(2);
  invoice["paymentStatus"] = "PAID";

  invoice["charges"] = [
    "Initial: $435.23 (TB-XXXX Initial)",
    "Balance: 1621.12 (TB-XXXX Adjustment)",
  ];
  invoice["invoiceNotes"] = "Payment Method: Card ending in 9654";
  invoice["promos"] = [];
  if (order.Type !== "Subscription") {
    invoice["promos"] = [
      "You Saved: $123 in fees by staying on site. Cheers!",
      "GET THIS ORDER FOR FREE. Sign up for a TOOLBX subscription. Visit toolbx.com/subscription for more details",
    ];
  }

  //   console.log("Invoice: ", invoice);
  return invoice;
};

const invoice = {
  shipping: {
    name: "John Doe",
    address: "1234 Main Street",
    city: "San Francisco",
    state: "CA",
    country: "US",
    postal_code: 94111,
  },
  items: [
    {
      item: "TC 100",
      description: "Toner Cartridge",
      quantity: 2,
      amount: 6000,
    },
    {
      item: "USB_EXT",
      description: "USB Cable Extender",
      quantity: 1,
      amount: 2000,
    },
  ],
  subtotal: 8000,
  paid: 0,
  invoice_nr: 1234,
};

module.exports = {
  generateModel,
};
