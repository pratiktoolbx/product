const fs = require("fs");
const PDFDocument = require("pdfkit");
const moment = require("moment");
const HR_GAP = 5;
const LINE_GAP = 14;
const FONT_SIZE_SML = 9;
const FONT_BOLD = "Helvetica-Bold";
const FONT_REG = "Helvetica";
const FONT_SIZE_REG = 10;
const FONT_SIZE_LRG = 12;

let currentY = 0;
let packingslip = true;
function createInvoice(invoice, path) {
  let doc = new PDFDocument({ size: "A4", margin: 50 });
  if (
    invoice.orderStatus === "Delivered" ||
    invoice.orderStatus === "Invoiced" ||
    invoice.orderStatus === "Paid" ||
    invoice.orderStatus === "Approved For Billing" ||
    invoice.type === "Subscription"
  ) {
    packingslip = false;
  }

  generateHeader(doc);

  generateCustomerInformation(doc, invoice);
  if (invoice.type === "Site to Site" && packingslip) {
    generateDropoff(doc, invoice);
  }
  if (!packingslip) {
    generateInvoiceTable(doc, invoice);
  }

  generateNotes(doc, invoice);
  // generateFooter(doc);

  doc.end();
  doc.pipe(fs.createWriteStream(path));
}

// HEADER ==========================
function generateHeader(doc) {
  doc.image("logo.png", 50, 45, { align: "center", width: 250 });
  if (!packingslip) {
    doc
      .fontSize(7)
      .text("325 FRONT ST. W, 4TH FLOOR TORONTO, ON, M5V 2Y1 ", 300, 55, {
        align: "right",
        width: 250,
      })
      .text("WWW.TOOLBX.COM | BILLING@TOOLBX.COM | 905-399-7572 ", 300, 65, {
        align: "right",
        width: 250,
      })
      .text("TOOLBX INC. | HST: 74701 5923 RT0001", 300, 75, {
        align: "right",
        width: 250,
      })
      .moveDown();
  }
  currentY = 140;
}

// CUSTOMER INFO ==========================
function generateCustomerInformation(doc, invoice) {
  // doc.fillColor("#444444").fontSize(20).text("Invoice", 50, 160);

  generateHr(doc, 120);

  const customerInformationTop = 130;

  currentY = 130;

  doc.fontSize(FONT_SIZE_REG).font(FONT_BOLD);
  doc.text(packingslip ? "Packing Slip" : "Invoice Details", 50, currentY);
  currentY += LINE_GAP;
  doc.text(packingslip ? "Delivery Number:" : "Invoice Number:", 50, currentY);
  doc.font(FONT_REG).text(invoice.invoice_nr, 150, currentY);

  currentY += LINE_GAP;

  doc.font(FONT_BOLD).text("Date:", 50, currentY);
  doc
    .fontSize(FONT_SIZE_REG)
    .text(formatDate(invoice.effectiveDate), 150, currentY);

  currentY += LINE_GAP;
  doc.fontSize(FONT_SIZE_REG).text(packingslip ? "" : "Total:", 50, currentY);
  doc
    .fontSize(FONT_SIZE_REG)
    .text(packingslip ? "" : formatCurrency(invoice.total), 150, currentY);

  currentY += LINE_GAP;
  if (invoice.type !== "Subscription") {
    let to = packingslip
      ? `Store: ${invoice.account}`
      : `To: ${invoice.account}`;

    doc
      .font(FONT_BOLD)
      .fontSize(FONT_SIZE_REG)
      .text(to, 300, customerInformationTop)
      .font(FONT_REG)
      .text(invoice.customer.address, 300, customerInformationTop + 15)
      .text(
        invoice.customer.city +
          ", " +
          invoice.customer.state +
          ", " +
          invoice.customer.country,
        300,
        customerInformationTop + 30
      )
      .moveDown();
  } else {
    let to = packingslip
      ? `Store: ${invoice.account}`
      : `To: ${invoice.account}`;

    doc
      .font(FONT_BOLD)
      .fontSize(FONT_SIZE_REG)
      .text(to, 300, customerInformationTop)
      .font(FONT_REG)
      .text(invoice.orderedBy, 300, customerInformationTop + 15)
      .moveDown();
  }

  generateHr(doc, currentY);

  // currentY = 270;
}

// CUSTOMER INFO ==========================
function generateDropoff(doc, invoice) {
  // doc.fillColor("#444444").fontSize(20).text("Invoice", 50, 160);

  currentY += 25;
  generateHr(doc, currentY);
  currentY += HR_GAP;
  doc
    .fontSize(FONT_SIZE_REG)
    .font(FONT_BOLD)
    .text("Dropoff Details", 50, currentY);

  currentY += LINE_GAP;
  doc.font(FONT_REG).fontSize(FONT_SIZE_REG).text("Contact:", 50, currentY);
  doc
    .font(FONT_BOLD)
    .fontSize(FONT_SIZE_LRG)
    .text(titleCase(invoice.dropoffName), 150, currentY);

  doc.font(FONT_REG).text(invoice.dropoffNotes, 300, currentY).moveDown();

  currentY += LINE_GAP;
  doc.font(FONT_REG).fontSize(FONT_SIZE_REG).text("Phone:", 50, currentY);
  doc
    .font(FONT_BOLD)
    .fontSize(FONT_SIZE_LRG)
    .text(invoice.dropoffPhoneNumber, 150, currentY);

  currentY += LINE_GAP;
  // currentY += LINE_GAP;
  doc.font(FONT_REG).fontSize(FONT_SIZE_REG);
  doc.text("Address:", 50, currentY);
  doc.text(invoice.to.address, 150, currentY);

  currentY += LINE_GAP;
  doc
    .text(
      invoice.to.city + ", " + invoice.to.state + ", " + invoice.to.country,
      150,
      currentY
    )
    .moveDown();

  currentY += LINE_GAP;

  generateHr(doc, currentY);

  // currentY = 270;
}

// PRODUCT LIST ==========================
function checkInvoiceNewPage(doc, position) {
  if (position > 700) {
    doc.addPage();
    generateTableRow(
      doc,
      100,
      "Product",
      "Unit Cost",
      "Quantity",
      "Line Total"
    );
    generateHr(doc, 120);
    currentY = 120;
    return true;
  }
  return false;
}

function generateTableRowForProducts(
  doc,
  invoice,
  invoiceTableTop,
  includeCharges = true
) {
  let totalNewPages = 0;
  let currentRowForNewPage = 0;
  let multiple = 0;
  for (i = 0; i < invoice.items.length; i++) {
    const item = invoice.items[i];
    if (totalNewPages > 0) {
      multiple = i - (currentRowForNewPage + 1);
    } else {
      multiple = i;
    }

    const position = invoiceTableTop + (multiple + 1) * 30;
    if (includeCharges) {
      generateTableRow(
        doc,
        position,
        item.product,
        formatCurrency(item.unitPrice),
        item.quantity,
        formatCurrency(item.totalPrice)
      );
    } else {
      generateTableRow(doc, position, item.product, "-", item.quantity, "-");
    }

    // check for new page (72 points per inch)
    if (checkInvoiceNewPage(doc, position)) {
      invoiceTableTop = 100;
      totalNewPages += 1;
      currentRowForNewPage = i;
    } else {
      generateHr(doc, position + 20);
      currentY = position + 30;
    }
  }
  return invoiceTableTop;
}

function generateInvoiceTable(doc, invoice) {
  let i;
  let invoiceTableTop = currentY + 30;

  doc.font(FONT_BOLD);
  generateTableRow(
    doc,
    invoiceTableTop,
    "Product",
    "Unit Cost",
    "Quantity",
    "Line Total"
  );
  generateHr(doc, invoiceTableTop + 20);
  doc.font(FONT_REG);

  // for a packing slip, we won't include product line items with costs
  console.log("STatus; ", invoice.orderStatus === "Paid");
  generateTableRowForProducts(doc, invoice, invoiceTableTop, true);
  generateTableRow(doc, currentY, "", "Subtotal", "");
  checkInvoiceNewPage(doc, currentY);
  currentY += 20;
  generateTableRow(doc, currentY, "", "Tax", "", formatCurrency(invoice.tax));
  checkInvoiceNewPage(doc, currentY);
  currentY += 20;
  generateTableRow(
    doc,
    currentY,
    "",
    "Total",
    "",
    formatCurrency(invoice.total)
  );
  checkInvoiceNewPage(doc, currentY);
  currentY += 20;

  doc.font(FONT_BOLD);
  generateTableRow(doc, currentY, "", "Status", "", invoice.paymentStatus);
  doc.font(FONT_REG);
}

function generateTableRow(doc, y, product, unitCost, quantity, lineTotal) {
  doc
    .fontSize(FONT_SIZE_SML)
    .text(product, 50, y)
    .text(unitCost, 280, y, { width: 90, align: "right" })
    .text(quantity, 370, y, { width: 90, align: "right" })
    .text(lineTotal, 0, y, { align: "right" });
}

// FOOTER ==========================
function generateFooter(doc) {
  doc
    .fontSize(FONT_SIZE_REG)
    .text(
      "Payment is due within 15 days. Thank you for your business.",
      50,
      780,
      { align: "center", width: 500 }
    );
}

// NOTES LIST ==========================
function checkNewNotesPage(doc) {
  if (currentY > 700) {
    doc.addPage();
    currentY = 100;
    doc
      .fillColor("#444444")
      .fontSize(FONT_SIZE_REG)
      .text("Notes", 50, currentY);
    currentY += LINE_GAP;
    generateHr(doc, currentY);
    currentY += HR_GAP;
    return true;
  }
  return false;
}

function addPONumberToNotes(doc, invoice) {
  // PO Number
  generateHr(doc, currentY);
  currentY += HR_GAP;
  doc
    .fontSize(FONT_SIZE_LRG)
    .font(FONT_BOLD)
    .text("PO Number: " + invoice.poNumber, 50, currentY);
  currentY += LINE_GAP;
}

function addPickupLocationToNotes(doc, invoice) {
  // Pickup Location
  generateHr(doc, currentY);
  currentY += HR_GAP;
  doc
    .fontSize(FONT_SIZE_SML)
    .font(FONT_BOLD)
    .text("Pick Up:", 50, currentY)
    .font(FONT_REG)
    .text(invoice.pickupAddressName, 100, currentY);
  currentY += LINE_GAP;
  doc.text(invoice.pickupAddressLocation, 100, currentY);
  currentY += LINE_GAP;
}

function addDropoffLocationToNotes(doc, invoice) {
  generateHr(doc, currentY);
  currentY += HR_GAP;
  let to = packingslip
    ? `Dropoff: ${titleCase(invoice.dropoffName)} (${
        invoice.dropoffPhoneNumber
      })`
    : `To: ${invoice.account}`;

  doc.font(FONT_BOLD).fontSize(FONT_SIZE_SML).text("Dropoff:", 50, currentY);
  currentY += LINE_GAP;
  doc
    .font(FONT_BOLD)
    .fontSize(FONT_SIZE_LRG)
    .text(titleCase(invoice.dropoffName), 50, currentY);

  currentY += LINE_GAP;
  doc
    .font(FONT_BOLD)
    .fontSize(FONT_SIZE_LRG)
    .text(invoice.dropoffPhoneNumber, 50);

  currentY += LINE_GAP;
  doc
    .font(FONT_REG)
    .fontSize(FONT_SIZE_REG)
    .text(invoice.to.address, 50, currentY);

  currentY += LINE_GAP;
  doc
    .text(
      invoice.to.city + ", " + invoice.to.state + ", " + invoice.to.country,
      50,
      currentY
    )
    .moveDown();
  currentY += LINE_GAP;
}
function addTextToNotes(doc, title, text) {
  // Material List
  generateHr(doc, currentY);
  currentY += HR_GAP;
  let lines = text.split(/\r\n|\r|\n/);
  doc.fontSize(FONT_SIZE_SML).font(FONT_BOLD).text(`${title}:`, 50, currentY);
  currentY += LINE_GAP;
  for (let i of lines) {
    doc.fontSize(FONT_SIZE_SML).font(FONT_REG).text(i, 50, currentY);
    currentY += LINE_GAP;
  }
}

function generateNotes(doc, invoice) {
  currentY = currentY + 30;
  if (!checkNewNotesPage(doc)) {
    doc
      .fillColor("#444444")
      .fontSize(FONT_SIZE_REG)
      .text("Notes", 50, currentY);
    currentY += LINE_GAP;
    generateHr(doc, currentY);
  }

  if (invoice.type === "Delivery Only") {
    addPONumberToNotes(doc, invoice);
    addPickupLocationToNotes(doc, invoice);
    addTextToNotes(doc, "Material Notes", invoice.materialList);
    generateHr(doc, currentY);
    currentY += HR_GAP;
  }

  if (invoice.type === "Site to Site") {
    if (invoice.poNumber) addPONumberToNotes(doc, invoice);
    addTextToNotes(doc, "Material Notes", invoice.materialList);
    generateHr(doc, currentY);
    currentY += HR_GAP;
  }

  if (!packingslip) {
    // charges
    currentY += HR_GAP;
    doc
      .fontSize(FONT_SIZE_SML)
      .font(FONT_REG)
      .text(invoice.invoiceNotes, 50, currentY);
    currentY += LINE_GAP;

    // charges
    generateHr(doc, currentY);
    currentY += HR_GAP;
    doc
      .fontSize(FONT_SIZE_SML)
      .font(FONT_BOLD)
      .text("Charges Placed:", 50, currentY);
    currentY += LINE_GAP;
    for (let c of invoice.charges) {
      doc.fontSize(FONT_SIZE_SML).font(FONT_REG).text(c, 50, currentY);
      currentY += LINE_GAP;
      checkNewNotesPage(doc);
    }

    for (let p of invoice.promos) {
      generateHr(doc, currentY);
      currentY += HR_GAP;
      doc.fontSize(FONT_SIZE_SML).font(FONT_BOLD).text(p, 50, currentY);
      currentY += LINE_GAP;
      checkNewNotesPage(doc);
    }
  }
}

// UTILITIES ==========================
function generateHr(doc, y) {
  doc.strokeColor("#cccccc").lineWidth(1).moveTo(50, y).lineTo(550, y).stroke();
}

function formatCurrency(cost) {
  return "$" + cost;
}

function formatDate(date) {
  return moment(date).format("MMMM Do, YYYY");
}

function titleCase(str) {
  var splitStr = str.toLowerCase().split(" ");
  for (var i = 0; i < splitStr.length; i++) {
    // You do not need to check if i is larger than splitStr length, as your for does that for you
    // Assign it back to the array
    splitStr[i] =
      splitStr[i].charAt(0).toUpperCase() + splitStr[i].substring(1);
  }
  // Directly return the joined string
  return splitStr.join(" ");
}

module.exports = {
  createInvoice,
};
