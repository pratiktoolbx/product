## Entry Points

- `node server.js` - will start a server that transcribes receipts into line items.
- `node app.js` - will download receipts from FrontApp (currently from HD) transcribe them, match them against the ordered items, and provide you stats. All catalog matches are outputted to out.csv.

## Walkthroughs

- Scaling Invoicing + Transcription Plan: [https://www.loom.com/share/8d2d5d02d6614c55932a0b7e997164ac](https://www.loom.com/share/8d2d5d02d6614c55932a0b7e997164ac)

- Prototype walkthrough: [https://www.loom.com/share/30c84fb3e8fb47dd883f7347385b66ac](https://www.loom.com/share/30c84fb3e8fb47dd883f7347385b66ac)

## Installation

1. `npm install`
2. Install using these instructions: [https://www.npmjs.com/package/pdf-ocr](https://www.npmjs.com/package/pdf-ocr)
3. Search for `-psm` into `--psm` in pdf-ocr module
