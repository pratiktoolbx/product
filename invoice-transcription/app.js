const match = require("./src/utils/match");
const sf = require("./src/utils/salesforce");
const HD = "HD";
const RONA = "RONA";
const DTL = "DTL";
const NCL = "NCL";
const fs = require("fs").promises;
const skus = require("./src/utils/skus");
const front = require("./src/utils/frontapp");
const hd = require("./src/scrapers/hd");
const dtl = require("./src/scrapers/dtl");
const rona = require("./src/scrapers/rona");
const ncl = require("./src/scrapers/ncl");

let totalHighConf = 0;
let totalMedConf = 0;
let totalLowConf = 0;

let highConfOrder = 0;
let lowConfOrder = 0;
const scrapeProductsFromFile = async (supplier, filepath) => {
  let products;
  if (supplier === HD) {
    products = await hd.getProductsFromFile(filepath);
  } else if (supplier === DTL) {
    products = await dtl.getProductsFromFile(filepath);
  } else if (supplier === RONA) {
    products = await rona.getProductsFromFile(filepath);
  } else if (supplier === NCL) {
    products = await ncl.getProductsFromFile(filepath);
  } else {
    console.log("File not found");
  }
  return products;
};

const matchProductsToReceipts = (supplier, receipt, materialOrdered, skuDB) => {
  try {
    let results = [];
    let materialOrderedCount = 0;
    for (let p of receipt.products) {
      if (materialOrderedCount >= materialOrdered.length) {
        console.log("materila ordered counted too many");
        break;
      }
      let orderedProducts = materialOrdered.map((m) => {
        return {
          ...m,
          productName: m.Invoice_Product_Name__c,
          qty: m.Quantity,
          price: m.App_Quote__c,
        };
      });
      let { bestMatch, filteredProducts, type } = match.getBestMatch(
        p.productName,
        p.sku,
        orderedProducts,
        skuDB
      );

      // console.log("Best match: ", bestMatch);

      for (let m of filteredProducts) {
        if (m.productName.toLowerCase() === bestMatch.target.toLowerCase()) {
          // console.log(
          //   `Ordered: ${m.Invoice_Product_Name__c} | Quote: ${parseFloat(
          //     m.App_Quote__c
          //   ).toFixed(2)} | Qty: ${m.Quantity} | ${m.Product2.ExternalId}`
          // );

          // console.log(
          //   `Receipt: ${p.productName} | SKU: ${p.sku} | Price: ${p.price} | Qty: ${p.qty}`
          // );

          // console.log("targets match:", m.Product2.ExternalId);

          let receiptPrice = parseFloat(m.App_Quote__c).toFixed(2);

          if (parseInt(p.qty) === parseInt(m.Quantity)) {
            if (type === "sku") {
              totalHighConf += 1;
            } else if (
              parseFloat(p.price) > receiptPrice * 0.95 &&
              parseFloat(p.price) < receiptPrice * 1.05
            ) {
              totalHighConf += 1;
            } else if (
              parseFloat(p.price) > receiptPrice * 0.85 &&
              parseFloat(p.price) < receiptPrice * 1.15
            ) {
              totalMedConf += 1;
            }
          } else {
            totalLowConf += 1;
          }

          if (m.Product2.ExternalId) {
            // console.log("CSV: ", m.Product2.ExternalId);

            let csvLine = [
              m.Product2.ExternalId,
              m.Invoice_Product_Name__c,
              m.Order.OrderNumber,
              supplier,
              receiptPrice,
              m.Quantity,
              p.sku.trim(),
              p.productName,
              p.price,
              p.qty,
              p.qty === m.Quantity,
              (receiptPrice - p.price) / p.price,
              type,
              bestMatch.rating,
            ];

            results.push(csvLine);
          }

          // console.log("========");
          break;
        }
      }
      materialOrderedCount += 1;
    }

    if (totalLowConf > 0) lowConfOrder += 1;
    else highConfOrder += 1;

    totalLowConf = 0;
    totalHighConf = 0;
    totalMedConf = 0;

    return results;
  } catch (e) {
    console.log("ERror in matching: ", e);
    return [];
  }
};

(async () => {
  try {
    let files = await front.loadReceipts(100);
    // let files = [
    //   {
    //     type: HD,
    //     filename: "TB-27641",
    //     toolbxNumber: 27641,
    //   },
    // ];
    console.log("======================================================");
    await sf.connect();

    let skuDB = await skus.loadSKUs();

    let totalHDOrders = 0;
    let totalZeroProducts = 0;
    let totalLinesMatch = 0;
    let totalOrdersWithCustomProducts = 0;
    let totalProducts = 0;

    let receipts = [];
    for (let f of files) {
      let materialOrdered = await sf.getOrderItems(`TB-${f.toolbxNumber}`);

      if (f.type === HD) totalHDOrders += 1;

      if (materialOrdered.length === 0) {
        console.log("Material Product == 0");
        continue;
      }
      console.log("scraping  ", f.filename);
      let receipt = await scrapeProductsFromFile(
        f.type,
        `./frontpdfs/${f.filename}.pdf`
      );

      if (receipt.products.length === 0) {
        totalZeroProducts += 1;
        console.log("Products Length == 0");
        continue;
      }
      console.log(
        `Parsing ${f.type} ${f.filename}. Products found: ${receipt.products.length}. Materials Ordered: ${materialOrdered.length}`
      );

      if (receipt.products.length === materialOrdered.length) {
        totalLinesMatch += 1;
        for (let m of materialOrdered) {
          if (!m.Product2.ExternalId) {
            totalOrdersWithCustomProducts += 1;
            break;
          }
        }
        totalProducts += receipt.products.length;
      }
      receipts.push(
        matchProductsToReceipts(f.type, receipt, materialOrdered, skuDB)
      );
      receipt = null;
    }
    let s =
      "P_ID|P_NAME|ORDER_ID|SUPPLIER|A_PRICE|A_QTY|SKU|R_NAME|R_PRICE|R_QTY|QTY_SAME|PRICE_DIFF|MATCH_TYPE|MATCH_RATING";

    // console.log("receipts: ", receipts);
    console.log("Total HD Receipts: ", totalHDOrders);
    console.log("Total Receipts With No Products: ", totalZeroProducts);
    console.log("Total Custom Prod Orders: ", totalOrdersWithCustomProducts);
    console.log("Total Receipts Line Matching: ", totalLinesMatch);
    console.log("Total Products: ", totalProducts);
    console.log("Total High Conf Orders: ", highConfOrder);
    console.log("Total Low Conf Orders: ", lowConfOrder);

    for (let r of receipts) {
      for (let l of r) {
        s += "\n";
        s += l.join("|");
      }
    }

    await fs.writeFile("./out.csv", s, "utf-8");

    console.log("done");
  } catch (e) {
    console.log("error: with filename", e);
  }
})();
