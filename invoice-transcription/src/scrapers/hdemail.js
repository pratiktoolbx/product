var pdfreader = require("pdfreader");
var Rule = pdfreader.Rule;

const nbCols = 8;
const cellPadding = 20; // each cell is padded to fit 40 characters
const columnQuantitizer = (item) => parseFloat(item.x) >= 20;
const filename = "../pdfs/hdemail_25131.pdf";

const padColumns = (array, nb) =>
  Array.apply(null, { length: nb }).map((val, i) => array[i] || []);
// .. because map() skips undefined elements

const mergeCells = (cells) => {
  //   console.log("Cells: ", cells);
  (cells || [])
    .map((cell) => cell.text)
    .join("") // merge cells
    .substr(0, cellPadding)
    .padEnd(cellPadding, " "); // padding
};

const renderMatrix = (matrix) => {
  //   console.log("Matrix: ", matrix);
  (matrix || [])
    .map((row, y) => padColumns(row, nbCols).map(mergeCells).join(" | "))
    .join("\n");
};

const printContent = (content) => {
  console.log("COntent: ", content);
};

var table = new pdfreader.TableParser();
var page = 0;
var start = false;
var end = false;
let counter = 1;

var detailsTemplate = {
  brand: null,
  productName: null,
  sku: null,
  qty: null,
  price: null,
  totalPrice: null,
};
var products = [];
var order = {
  subtotal: null,
  total: null,
  products: products,
};

let lineDetails = { ...detailsTemplate };
let lineBegins = false;
let currentLine = 0;
new pdfreader.PdfReader().parseFileItems(filename, function (err, item) {
  if (!item || item.page) {
    // end of file, or page
    if (item) {
      page = item.page;
    }

    // table = new pdfreader.TableParser(); // new/clear table for next page
  } else if (item.text) {
    if (page === 1 && start && !end) {
      // processItem(item);
      // accumulate text items into rows object, per line

      let text = item.text.trim();

      if (
        parseInt(text) == currentLine + 10 ||
        (parseFloat(text) < 100 && parseFloat(text) % 10 === 0)
      ) {
        currentLine = currentLine + 10;
        console.log("NEW LINE");
        products.push(lineDetails);
        lineDetails = { ...detailsTemplate };
        counter = 1;
      }

      console.log("item.text counter currentline", text, counter, currentLine);
      switch (counter) {
        case 2: {
          lineDetails.sku = text;
        }
        case 3: {
          lineDetails.productName = text;
        }
        case 4: {
          lineDetails.qty = text;
        }
        case 7: {
          lineDetails.price = text;
        }
        case 8: {
          lineDetails.totalPrice = text;
        }
        default:
          null;
      }

      counter += 1;

      // table.processItem(item, columnQuantitizer(item));
    }

    if (item.text.trim() === "END OF CARRY OUT") {
      end = true;
      console.log("Products; ", products);
    }
    if (item.text.trim() === "CARRY OUT MERCHANDISE:") start = true;
  }
});

// var processItem = Rule.makeItemProcessor([
//   Rule.on(/^CARRY OUT MERCHANDISE:$/)
//     .parseTable(5, false)
//     .then(printContent),
//   Rule.on(/^Job Desc. /)
//     .parseNextItemValue()
//     .then(printContent),
// ]);
// new pdfreader.PdfReader().parseFileItems(filename, function (err, item) {
//   processItem(item);
// });
