const ocr = require("../utils/ocr");
const scrapeUtil = require("../utils/scrapeUtil");
const resultModel = require("../models/result");

const getProductsFromFile = async (filename) => {
  let result = resultModel.template;
  result = {
    total: null,
    subtotal: null,
    tax: null,
    products: [],
  };
  try {
    let text = await ocr.getProductsViaOCR(filename, "ocr");
    //   console.log("Text: ", text.textPages[0].split("\n"));
    let textArray = text.textPages[0].split("\n");

    let linesStarted = false;
    let linesEnded = false;

    for (let t of textArray) {
      let itemsRow = t.split(" ");
      if (linesStarted && !linesEnded) {
        // check if enough items in row
        if (itemsRow.length > 8) {
          let productName = itemsRow.slice(4, itemsRow.length - 4).join(" ");
          let [price, total] = scrapeUtil.last(itemsRow, 2).map((i) => {
            return parseFloat(scrapeUtil.removeChars(i));
          });
          let sku = itemsRow[2];
          let qty = parseInt(scrapeUtil.removeChars(itemsRow[1]));

          console.log(qty, price, total);
          // don't have a qty or price that makes sense
          if (isNaN(price) || isNaN(qty)) continue;

          result.products.push({
            productName,
            sku,
            qty,
            price,
            total,
          });
        }
      }

      if (t.indexOf("YOU SAVED") > -1) linesEnded = true;
      if (t.indexOf("LT Qty SKU") > -1) linesStarted = true;

      if (linesEnded) {
        let [label, value] = scrapeUtil.last(itemsRow, 2);
        if (label === "SUBTOTAL") {
          let num = parseFloat(scrapeUtil.removeChars(value));
          if (!isNaN(num)) result.subtotal = num;
        }
        if (label === "GST/HST") {
          let num = parseFloat(scrapeUtil.removeChars(value));
          if (!isNaN(num)) result.tax = num;
        }
        if (label === "TOTAL") {
          let num = parseFloat(scrapeUtil.removeChars(value));
          if (!isNaN(num)) result.total = num;
        }
      }
    }
  } catch (e) {
    console.log("Error when trying to parse RONA", e);
  }

  return result;
};

module.exports = {
  getProductsFromFile,
};
