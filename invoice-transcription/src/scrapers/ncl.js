const ocr = require("../utils/ocr");
const scrapeUtil = require("../utils/scrapeUtil");
const resultModel = require("../models/result");

const getProductsFromFile = async (filename) => {
  let result = resultModel.template;
  result = {
    total: null,
    subtotal: null,
    tax: null,
    products: [],
  };
  try {
    let text = await ocr.getProductsViaOCR(filename, "ocr");
    let textArray = text.textPages[1].split("\n");

    let linesStarted = false;
    let linesEnded = false;

    for (let t of textArray) {
      if (linesStarted && !linesEnded) {
        let itemsRow = t.split(" ");
        if (isNaN(parseFloat(scrapeUtil.last(itemsRow)))) continue;
        // check if enough items in row
        if (itemsRow.length > 5) {
          let endProduct = itemsRow.length - 4;
          let productName = itemsRow.slice(1, endProduct).join(" ");
          let [qty, unitType, price, total] = scrapeUtil.last(itemsRow, 4);
          let sku = itemsRow[0];
          result.products.push({
            productName,
            sku,
            qty: parseInt(scrapeUtil.removeChars(qty)),
            price: parseFloat(scrapeUtil.removeChars(price)),
            total: parseFloat(scrapeUtil.removeChars(total)),
          });
        }
      }

      if (t.indexOf("* Charge ") === 0) linesEnded = true;
      if (t.indexOf("*** WALK-IN ORDER ***") > -1) linesStarted = true;
    }
  } catch (e) {
    console.log("Error when trying to parse NCL", e);
  }

  return result;
};

module.exports = {
  getProductsFromFile,
};
