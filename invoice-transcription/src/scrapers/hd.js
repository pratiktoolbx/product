var pdfreader = require("pdfreader");
const scrapeUtil = require("../utils/scrapeUtil");
const resultModel = require("../models/result");
const line = require("../models/line");

const readPDFPromise = async (filename) => {
  return await new Promise((resolve, reject) => {
    let result = { ...resultModel.template };
    // console.log("resultModel.template: ", resultModel.template);
    result = {
      total: null,
      subtotal: null,
      tax: null,
      products: [],
    };
    var page = 0;
    var start = false;
    var end = false;

    let lineDetails = { ...line.template };
    let startLineNumber = false;
    let startSKU = false;
    let startProductName = false;
    let startQty = false;
    let startUoM = false;
    let startTax = false;
    let startPrice = false;
    let startTotalLine = false;
    let startSubtotal = false;
    let startTotal = false;
    let startTotalTax = false;
    new pdfreader.PdfReader().parseFileItems(filename, (err, item) => {
      if (err) {
        console.log("Rejected", err);
        reject();
      } else if (!item) {
        // console.log("HD Result: ", result.products.length);
        resolve(result);
      } else if (item.page) {
        // end of file, or page
        if (item) {
          page = item.page;
        }

        // table = new pdfreader.TableParser(); // new/clear table for next page
      } else if (item.text) {
        let text = item.text.trim();
        // console.log("got text: ", text);
        if (page === 1 && start && !end) {
          // processItem(item);
          // accumulate text items into rows object, per line

          if (startLineNumber) {
            startLineNumber = false;
            startSKU = true;
          } else if (startSKU) {
            lineDetails.sku = text;
            startSKU = false;
            startProductName = true;
          } else if (startProductName) {
            if (/[a-zA-Z]/.test(text)) {
              lineDetails.productName += text;
            } else {
              startProductName = false;
              startQty = true;
            }
          }
          if (startQty) {
            lineDetails.qty = parseFloat(scrapeUtil.removeChars(text));
            startQty = false;
            startUoM = true;
          } else if (startUoM) {
            startUoM = false;
            startTax = true;
          } else if (startTax) {
            startTax = false;
            startPrice = true;
          } else if (startPrice) {
            lineDetails.price = parseFloat(scrapeUtil.removeChars(text));
            startPrice = false;
            startTotalLine = true;
          } else if (startTotalLine) {
            lineDetails.total = parseFloat(scrapeUtil.removeChars(text));
            startTotalLine = false;
            startLineNumber = true;
            if (lineDetails.sku.indexOf("100") === 0) {
              result.products.push(lineDetails);
            } else {
              console.log("HD: Couldn't find SKU properly", lineDetails);
            }

            lineDetails = { ...line.template };
          }
        }

        if (text === "END OF CARRY OUT") {
          end = true;
          console.log("End Carry");
        }
        if (text === "END OF CUSTOMER PICK UP") {
          end = true;
          console.log("END OF CUSTOMER PICK UP");
        }
        if (text === "Extension") {
          start = true;
          startLineNumber = true;
        }

        if (end && startSubtotal) {
          result.subtotal = parseFloat(scrapeUtil.removeChars(text));
          startSubtotal = false;
        }
        if (end && startTotalTax) {
          result.tax = parseFloat(scrapeUtil.removeChars(text));
          startTotalTax = false;
        }
        if (end && startTotal) {
          result.total = parseFloat(scrapeUtil.removeChars(text));
          startTotal = false;
        }

        if (text === "PRE-TAX TOTAL") startSubtotal = true;
        if (text === "HST") startTotalTax = true;
        if (text === "TOTAL") startTotal = true;
      }
    });
  });
};
const getProductsFromFile = async (filename) => {
  try {
    result = await readPDFPromise(filename);
    return result;
  } catch (e) {
    console.log("Error:", e);
  }
};

module.exports = {
  getProductsFromFile,
};
