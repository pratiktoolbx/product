var pdfreader = require("pdfreader");
var Rule = pdfreader.Rule;

const nbCols = 8;
const cellPadding = 20; // each cell is padded to fit 40 characters
const columnQuantitizer = (item) => parseFloat(item.x) >= 20;
const filename = "./pdfs/HDPRO_24946.pdf";

const padColumns = (array, nb) =>
  Array.apply(null, { length: nb }).map((val, i) => array[i] || []);
// .. because map() skips undefined elements

const mergeCells = (cells) => {
  //   console.log("Cells: ", cells);
  (cells || [])
    .map((cell) => cell.text)
    .join("") // merge cells
    .substr(0, cellPadding)
    .padEnd(cellPadding, " "); // padding
};

const renderMatrix = (matrix) => {
  //   console.log("Matrix: ", matrix);
  (matrix || [])
    .map((row, y) => padColumns(row, nbCols).map(mergeCells).join(" | "))
    .join("\n");
};

const printContent = (content) => {
  console.log("COntent: ", content);
};

var table = new pdfreader.TableParser();
var page = 0;

var purchaseLocation = null;
var start = false; // "Order Summary"
var end = false;

var readingBrand = false;
var readingProductName = false;
var readingQty = false;
var readingPrice = false;
var readingSKU = false;

var lineItemEnd = false;

var detailsTemplate = {
  brand: null,
  productName: null,
  sku: null,
  qty: null,
  price: null,
};
var products = [];
var order = {
  subtotal: null,
  total: null,
  products: products,
};

let lineDetails = { ...detailsTemplate };

const getProductsFromFile = async (filename) => {
  return new Promise((resolve, reject) => {
    new pdfreader.PdfReader().parseFileItems(filename, function (err, item) {
      if (!item || item.page) {
        // end of file, or page
        if (item) {
          // console.log("PAGE:", item.page);
          page = item.page;
        }

        // table = new pdfreader.TableParser(); // new/clear table for next page
      } else if (item.text) {
        let text = item.text.trim();
        if (start && !end) {
          // accumulate text items into rows object, per line

          if (text.indexOf("(/") > -1) {
            readingBrand = false;
            readingSKU = false;
            readingProductName = false;
          }

          if (readingSKU) {
            // console.log("item.text: ", text);
            lineDetails.sku = lineDetails.sku ? lineDetails.sku + text : text;
          }

          if (text.indexOf("SKU") > -1) {
            readingSKU = true;
            readingProductName = false;
            readingBrand = false;
            lineDetails.sku = lineDetails.sku ? lineDetails.sku + text : text;
          }

          if (readingPrice) {
            lineDetails.price = text;
            readingProductName = false;
            lineItemEnd = true;
          }
          if (readingQty) {
            lineDetails.qty = text;
            readingQty = false;
            readingPrice = true;
          }

          if (readingProductName) {
            lineDetails.productName = lineDetails.productName
              ? lineDetails.productName + text
              : text;

            lineDetails.productName = lineDetails.productName.split("(/")[0];
          }

          if (readingBrand) {
            lineDetails.brand = lineDetails.brand
              ? lineDetails.brand + text
              : text;
            if (text.length > 0 && text[0] === text[0].toUpperCase()) {
              readingBrand = false;
              readingProductName = true;
              lineDetails.productName = lineDetails.productName
                ? lineDetails.productName + text
                : text;
            }
          }

          if (text.indexOf("Qty") > -1) {
            readingQty = true;
          }

          if (lineItemEnd) {
            // if (lineDetails.price )
            products.push(lineDetails);
            // console.log(lineDetails);
            lineDetails = { ...detailsTemplate };
            lineItemEnd = false;
            readingBrand = true;
            readingProductName = false;
            readingPrice = false;
            readingQty = false;
            readingSKU = false;
          }

          //   table.processItem(ite           m, columnQuantitizer(item));
        }
        if (text.indexOf("Order Summary") > -1) {
          start = true;
          readingBrand = true;
        }
        if (text.indexOf("Subtotal") > -1) {
          start = false;
          end = true;
          resolve(products);
          // console.log("products: ", products);
        }
      }
    });
  });
};

module.exports = {
  getProductsFromFile,
};
