const ocr = require("../utils/ocr");
const scrapeUtil = require("../utils/scrapeUtil");
const resultModel = require("../models/result");

const getProductsFromFile = async (filename) => {
  let result = resultModel.template;
  result = {
    total: null,
    subtotal: null,
    tax: null,
    products: [],
  };
  try {
    let text = await ocr.getProductsViaOCR(filename, "text");
    let textArray = text.textPages[0].split("\n");

    let linesStarted = false;
    let linesEnded = false;

    for (let t of textArray) {
      // console.log("T: ", t);
      let itemsRow = t.split(" ");
      if (linesStarted && !linesEnded) {
        if (isNaN(parseFloat(scrapeUtil.last(itemsRow)))) continue;
        // check if enough items in row
        if (itemsRow.length > 8) {
          let productName = itemsRow.slice(2, itemsRow.length - 6).join(" ");
          let [sku, a, units, price, unitType, total] = scrapeUtil.last(
            itemsRow,
            6
          );
          let qty = itemsRow[1];

          result.products.push({
            productName,
            sku,
            qty: parseInt(scrapeUtil.removeChars(qty)),
            price: parseFloat(scrapeUtil.removeChars(price)),
            total: parseFloat(scrapeUtil.removeChars(total)),
          });
        }
      }

      if (t.indexOf("SUBTOTAL") === 0) linesEnded = true;
      if (t.indexOf("L# ") > -1) linesStarted = true;

      if (linesEnded) {
        let [label, value] = scrapeUtil.last(itemsRow, 2);
        if (label === "SUBTOTAL") {
          let num = parseFloat(scrapeUtil.removeChars(value));
          if (!isNaN(num)) result.subtotal = num;
        }
        if (label === "H.S.T.") {
          let num = parseFloat(scrapeUtil.removeChars(value));
          if (!isNaN(num)) result.tax = num;
        }
        if (label === "TOTAL") {
          let num = parseFloat(scrapeUtil.removeChars(value));
          if (!isNaN(num)) result.total = num;
        }
      }
    }
  } catch (e) {
    console.log("Error when trying to parse DTL", e);
  }

  return result;
};

module.exports = {
  getProductsFromFile,
};
