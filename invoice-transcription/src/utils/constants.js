// export const HDPRO = "HDPRO";
// export const HDEMAIL = "HDEMAIL";
// export const RONA = "RONA";
// export const DTL = "DTL";
// export const NCL = "NCL";

const HDPRO = "HDPRO";
const HDEMAIL = "HDEMAIL";
const RONA = "RONA";
const DTL = "DTL";
const NCL = "NCL";

module.export = {
  HDPRO,
  HDEMAIL,
  RONA,
  DTL,
  NCL,
};
