const fs = require("fs");
const pdf = require("pdf-parse");

const HD = "HD";
const RONA = "RONA";
const DTL = "DTL";
const NCL = "NCL";

const tbFileName = (toolbxNumber) => {
  const filename = `TB-${toolbxNumber}`;
  return filename;
};
const readFile = async (filename) => {
  return new Promise((resolve, reject) => {
    fs.readFile(filename, (err, data) => {
      if (err) reject(err);
      resolve(data);
    });
  });
};
const parseNewCanadianLumberReceipt = (c) => {
  const lines = c.last_message.text.split("\n");
  const line = lines.find((l) =>
    l.toLowerCase().startsWith("tb-".toLowerCase())
  );
  if (line) {
    const splits = line.split("-");
    const toolbxNumber = parseInt(splits[splits.length - 1]) || null;

    if (toolbxNumber) {
      if (c.last_message.attachments[0]) {
        return {
          type: NCL,
          filename: tbFileName(toolbxNumber),
          toolbxNumber,
          attachmentUrl: c.last_message.attachments[0].url,
        };
      }
    }
  }
  return null;
};

const parseHomeDepotReceipt = (c) => {
  const lines = c.last_message.text.split("\n");
  const line = lines.find((l) =>
    l
      .toLowerCase()
      .startsWith(
        "This e-mail includes an electronic copy of your order".toLowerCase()
      )
  );
  if (line) {
    const splits = line.split(" - ");
    const toolbxNumber = parseInt(splits[splits.length - 1]) || null;
    if (toolbxNumber) {
      if (c.last_message.attachments[0]) {
        return {
          type: HD,
          toolbxNumber,
          filename: tbFileName(toolbxNumber),
          attachmentUrl: c.last_message.attachments[0].url,
        };
      }
    }
  }
  return null;
};

const parseDowntownLumberReceipt = (c) => {
  //   console.log("DTL:", c);
  return {
    type: DTL,
    toolbxNumber: null,
    filename: null,
    attachmentUrl: c.last_message.attachments[0].url,
  };
};

const extractToolbxNumberFromDowntownLumberPDF = async (filename) => {
  const buffer = await readFile(filename);
  const parsed = await pdf(buffer);
  const text = parsed.text;
  const textBlocks = text.split(" ").filter((t) => !!t);
  const block = textBlocks.find((b) => b.startsWith("PO:TB-")); // PO:TB-12345
  return block ? parseInt(block.split("-")[1], 10) : null;
};

const parseRonaReceipt = (c) => {
  const trimmedSubject = c.subject.trim();
  const toolbxNumber = parseInt(
    trimmedSubject.slice(trimmedSubject.length - 5)
  );
  const pdfAttachments = c.last_message.attachments.filter((a) =>
    a.filename.toLowerCase().endsWith(".pdf")
  );
  if (toolbxNumber && pdfAttachments[0]) {
    return {
      type: RONA,
      toolbxNumber,
      filename: tbFileName(toolbxNumber),
      attachmentUrl: pdfAttachments[0].url,
    };
  }
  return null;
};

module.exports = {
  parseNewCanadianLumberReceipt,
  parseHomeDepotReceipt,
  parseDowntownLumberReceipt,
  extractToolbxNumberFromDowntownLumberPDF,
  parseRonaReceipt,
  tbFileName,
};
