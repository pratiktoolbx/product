const ss = require("string-similarity");

const getFilteredProductsForMaterialOrdered = (rp, mos) => {
  // rp = receiptProduct
  // mos = materials ordered
  let filteredProducts = [];

  filteredProducts = mos.filter((p) => {
    return rp.qty === p.Quantity;
  });

  if (filteredProducts.length === 0) {
    filteredProducts.push(
      ...mos.filter((p) => {
        return (
          rp.price > p.App_Quote__c * 0.85 && rp.price < p.App_Quote__c * 1.15
        );
      })
    );
  }
  if (filteredProducts.length === 0) {
    // couldn't find any matches, just default to the full list
    filteredProducts = mos;
  }

  return filteredProducts;
};

const getBestMatch = (material, materialSKU, productList, skuDB) => {
  // console.log(receiptProducts);
  // let filteredProducts = getFilteredProducts(material, productList);

  let type, matches;
  let bestMatch = {};
  let filteredProducts = [];
  // console.log("materialSKU:", materialSKU);
  if (skuDB[materialSKU] !== undefined) {
    type = "sku";
    // console.log("product list: ", productList);
    filteredProducts = productList.filter((p) => {
      // console.log("Match: ", p.Product2.ExternalId, skuDB[materialSKU].id);
      return p.Product2.ExternalId === skuDB[materialSKU].id;
    });

    // console.log("SKU Match: ", filteredProducts);
    bestMatch["target"] = skuDB[materialSKU].name;
    bestMatch["rating"] = 1;
  }

  if (filteredProducts.length === 0) {
    type = "fuzzy";
    filteredProducts = getFilteredProductsForMaterialOrdered(
      material,
      productList
    );
    let products = filteredProducts.map((p) => {
      return p.productName.toLowerCase();
    });
    let matches = ss.findBestMatch(material.toLowerCase(), products);
    return { bestMatch: matches.bestMatch, filteredProducts, type };
  } else {
    return { bestMatch, filteredProducts, type };
  }
};

module.exports = {
  getBestMatch,
};
