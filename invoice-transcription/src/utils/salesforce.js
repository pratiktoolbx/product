const jsforce = require("jsforce");
require("dotenv").config();

let conn = new jsforce.Connection({
  oauth2: {
    clientId: process.env.SALESFORCE_CLIENT_ID,
    clientSecret: process.env.SALESFORCE_CLIENT_SECRET,
    loginUrl: process.env.SALESFORCE_LOGIN_URI,
    redirectUri: process.env.SALESFORCE_REDIRECT_URI,
  },
});
conn.bulk.pollTimeout = 60000;

const connect = async () => {
  try {
    await conn.login(
      process.env.SALESFORCE_USERNAME,
      process.env.SALESFORCE_PASSWORD
    );
    console.log("Logged In: ", conn.accessToken);
  } catch (e) {
    console.log("Exception when trying to login: ", e);
  }
  return null;
};

const getOrderItems = async (OrderNumber) => {
  try {
    let result = await conn.query(
      `SELECT 
        Order.OrderNumber, 
        Order.EffectiveDate, 
        Order.Pickup_Location_Address__c, 
        Product2.ExternalId,
        Invoice_Product_Name__c, 
        Custom_Product_Name__c,
        OriginalOrderItemId,
        Quantity, 
        App_Quote__c, 
        Markup_Percentage__c, 
        Markup_Unit_Price__c, 
        Total_Markup_Amount__c 
      FROM 
        OrderItem 
      WHERE 
        Order.OrderNumber='${OrderNumber}'
        and Invoice_Product_Name__c <> 'TOOLBX Delivery'
        and Invoice_Product_Name__c <> 'TOOLBX Discount'`
    );

    // console.log("Material Ordered Length: ", result.records.length);
    return result.records;
  } catch (e) {
    console.error(e);
  }
  return null;
};

module.exports = {
  connect,
  getOrderItems,
};
