const axios = require("axios");
const fs = require("fs");
const su = require("./scrapeUtil");
const tb = require("./tbNumber");
require("dotenv").config();

const FRONT_APP_URL = "https://api2.frontapp.com";

const headers = {
  Authorization: `Bearer ${process.env.FRONTAPP_API_KEY}`,
};

async function downloadFile(convo) {
  const pathComponents = convo.attachmentUrl.split("/");
  let filename = null;

  if (!convo.filename) {
    filename = `./${pathComponents[pathComponents.length - 1]}`;
  } else {
    filename = convo.filename;
  }

  await su.savePDFFromURL(
    "./frontpdfs",
    filename,
    convo.attachmentUrl,
    headers
  );

  return {
    path: "./frontpdfs",
    filename: `${pathComponents[pathComponents.length - 1]}`,
  };

  // const res = await axios.get(convo.attachmentUrl, { headers });
  // const file = fs.createWriteStream(filename);

  // console.log("saving to file: ", filename);
  // await new Promise((resolve, reject) => {
  //   res.body.pipe(file);
  //   res.body.on("error", reject);
  //   file.on("finish", resolve);
  // });
}

const HD = "HD";
const RONA = "RONA";
const DTL = "DTL";
const NCL = "NCL";

const getSourceAndURL = (message) => {
  let source = null;
  // console.log("receipient", message.recipient);

  let r = {};
  if (message.recipient.role === "from") {
    if (message.recipient.handle === "info@homedepot.ca") {
      r = tb.parseHomeDepotReceipt(message);
    } else if (message.recipient.handle === "store@newcanadianslumber.com") {
      r = tb.parseNewCanadianLumberReceipt(message);
    } else if (message.recipient.handle === "info@downtownlumber.com") {
      r = tb.parseDowntownLumberReceipt(message);
    } else if (message.recipient.handle === "shaun.fernandes@rona.ca") {
      r = tb.parseRonaReceipt(message);
    }
  }

  return r;
};
async function loadReceipts(limit) {
  const res = await axios.get(
    `${FRONT_APP_URL}/inboxes/inb_8lx9/conversations?limit=${limit}`,
    { headers }
  );
  const body = await res.data;
  const results = body._results;
  const filteredResults = results; // results.map(extractInfo).filter(isNotNull);
  let message;
  let files = [];
  for (let i = 0; i < filteredResults.length; i++) {
    message = filteredResults[i];
    // console.log("subject: ", message.last_message.subject);
    // console.log("attachments: ", message.last_message.attachments);
    if (message.last_message.attachments.length === 0) {
      console.log("No attachment");
      continue;
    }
    if (
      message.last_message.attachments[0].content_type !== "application/pdf"
    ) {
      console.log("attachment is not a PDF");
      continue;
    }

    let r = getSourceAndURL(message);

    if (!r) {
      console.log("Couldn't get source and url");
      continue;
    }
    // console.log(r);
    if (!r.attachmentUrl) {
      // console.log("Couldn't get URL parts of attachment");
      // console.log("Attachments: ", message.last_message.attachments);
      continue;
    }
    if (r.type !== HD) continue;
    const file = await downloadFile(r);

    if (r.type === DTL) {
      r.toolbxNumber = await tb.extractToolbxNumberFromDowntownLumberPDF(
        `${file.path}/${file.filename}.pdf`
      );
      r.filename = tb.tbFileName(r.toolbxNumber);
      fs.renameSync(
        `${file.path}/${file.filename}.pdf`,
        `${file.path}/${r.filename}.pdf`
      );
    }
    console.log(message.recipient.handle, r.filename);
    files.push(r);
  }

  // console.log("message: ", message);

  console.log({
    message: "Completed loading receipts!",
    label: "Yay!",
  });
  return files;
}

module.exports = {
  loadReceipts,
};

// (async () => {
//   console.log("testing");
//   try {
//     await loadReceipts(100);
//   } catch (e) {
//     console.log("error ocurred", e);
//   }
// })();
