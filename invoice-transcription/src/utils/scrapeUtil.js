const axios = require("axios");
const fs = require("fs").promises;

const last = (array, n) => {
  if (array == null) return void 0;
  if (n == null) return array[array.length - 1];
  return array.slice(Math.max(array.length - n, 0));
};

const removeChars = (str) => {
  return str.replace(/[a-zA-Z$]/g, "");
};

const savePDFFromURL = async (pathToFile, filename, url, headers) => {
  try {
    let pdfResponse = await axios({
      method: "get",
      url: url,
      responseType: "arraybuffer",
      headers,
    });

    // check for error

    let path = `${pathToFile}/${filename}.pdf`;
    await fs.writeFile(path, pdfResponse.data);

    return true;

    // download pdf
    // parse text based on supplier
    // respond with products
  } catch (e) {
    console.log("ER: ", e);
    return false;
  }

  const handleParse = async (supplier, pdfURL) => {};
};
module.exports = {
  last,
  removeChars,
  savePDFFromURL,
};
