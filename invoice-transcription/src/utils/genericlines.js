var pdfreader = require("pdfreader");
const k = require("./constants");

const HDPRO = "HDPRO";
const HDEMAIL = "HDEMAIL";
const RONA = "RONA";
const DTL = "DTL";
const NCL = "NCL";

const checkFileType = async (filename) => {
  try {
    return new Promise((resolve, reject) => {
      new pdfreader.PdfReader().parseFileItems(filename, function (err, item) {
        if (!item || item.page) {
          // end of file, or page
          //   console.log(renderMatrix(table.getMatrix()));
          if (item) {
            // console.log("PAGE:", item.page);
            page = item.page;
          }

          //   table = new pdfreader.TableParser(); // new/clear table for next page
        } else if (item.text) {
          let text = item.text.trim();
          // console.log("text: ", text);
          if (text.indexOf("The Home Depot Canada") > -1) {
            // console.log("HDPRO SELECTED", HDPRO);
            resolve(HDPRO);
            // return HDPRO;
          }
          if (text.indexOf("Home Depot Canada Inc.") > -1) {
            resolve(HDEMAIL);
          }
          if (text.indexOf("RONA") > -1) {
            resolve(RONA);
          }
          if (text.indexOf("VISIT OUR LUMBER DIVISION AT #9 MORROW AVE") > -1) {
            resolve(DTL);
          }

          // resolve(null);
        }
      });
      // resolve(NCL);
      resolve(RONA);
    });
  } catch (e) {
    console.log("Error trying check file type: ", e);
    return null;
  }
};

const parseFile = async (filename, resolve, reject) => {};

module.exports = {
  checkFileType,
};
