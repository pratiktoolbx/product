const fs = require("fs");
const csv = require("csv-parser");

const loadSKUs = async () => {
  let results = [];
  await new Promise((resolve, reject) => {
    fs.createReadStream("./src/models/skus.csv")
      .pipe(csv())
      .on("data", (data) => results.push(data))
      .on("end", () => {
        resolve(results);
      });
  });

  let m = {};
  for (let r of results) {
    m[r.sku] = r;
  }

  return m;
};

module.exports = {
  loadSKUs,
};
