const genericlines = require("./utils/genericlines");
const fs = require("fs").promises;
const moment = require("moment");
const axios = require("axios");

const hd = require("./scrapers/hd");
const dtl = require("./scrapers/dtl");
const rona = require("./scrapers/rona");
const ncl = require("./scrapers/ncl");

const parserMap = {
  HD: hd,
  RONA: rona,
  DTL: dtl,
  NCL: ncl,
};
const handleParse = async (supplier, pdfURL) => {
  try {
    let pdfResponse = await axios({
      method: "get",
      url: pdfURL,
      responseType: "arraybuffer",
    });

    // check for error

    let path = `/tmp/${supplier}-${moment().format("YYYYMMDD")}.pdf`;
    await fs.writeFile(path, pdfResponse.data);

    let result = await parserMap[supplier].getProductsFromFile(path);
    console.log("receipt: ", result);
    return result;

    // download pdf
    // parse text based on supplier
    // respond with products
  } catch (e) {
    console.log("ER: ", e);
    return null;
  }
};

module.exports = {
  handleParse,
};
