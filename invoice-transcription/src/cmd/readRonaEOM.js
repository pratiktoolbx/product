var pdfreader = require("pdfreader");
var fs = require("fs");

(async () => {
  const filename = "../../pdfs/rona_may.pdf";
  const allLines = [];
  allLines.push("NUMBER,DATE,CHARGES/CREDIT,BALANCE,NUMBER,CHARGES,BALANCE");
  try {
    new pdfreader.PdfReader().parseFileItems(filename, function (err, item) {
      if (item && item.page) {
        // end of file, or page
        //   console.log(renderMatrix(table.getMatrix()));
        if (item) {
          console.log("PAGE:", item.page);
          console.log();
          page = item.page;
        }

        //   table = new pdfreader.TableParser(); // new/clear table for next page
      } else if (item && item.text) {
        let text = item.text.trim().replace(/ +(?= )/g, "");
        if (text.split(" ").length == 7) {
          allLines.push(text.replace(/ /g, ","));
          console.log("text: ", text.split(" ").length, text);
        }

        // resolve(null);
      } else if (!item) {
        console.log("Writing to file NOW");
        fs.writeFileSync("../../output/rona_may.csv", allLines.join("\n"));
        console.log("DONE NOW");
      }
    });
  } catch (e) {
    console.log("Error trying check file type: ", e);
    return null;
  }
})();
