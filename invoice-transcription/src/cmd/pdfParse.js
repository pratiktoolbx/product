const pdf = require("pdf-parse");
const fs = require("fs");
const readFile = async (filename) => {
  return new Promise((resolve, reject) => {
    fs.readFile(filename, (err, data) => {
      if (err) reject(err);
      resolve(data);
    });
  });
};

(async () => {
  const filename = "../../pdfs/HD_TB-25131.pdf";
  try {
    const buffer = await readFile(filename);
    const parsed = await pdf(buffer);
    const text = parsed.text;
    console.log("text: ", text);
  } catch (e) {
    console.log("Error trying check file type: ", e);
    return null;
  }
})();
