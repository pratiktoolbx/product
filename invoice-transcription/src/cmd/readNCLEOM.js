var inspect = require("eyes").inspector({ maxLength: 20000 });
var pdf_extract = require("pdf-ocr");
var fs = require("fs");
// var absolute_path_to_pdf = "../../pdfs/NCL_TB-24538.pdf";
// var absolute_path_to_pdf = "./pdfs/NCL_TB-24333.pdf";
var absolute_path_to_pdf = "../../pdfs/ncl_may.pdf";
// var absolute_path_to_pdf = "./pdfs/RONA_TB-25641.pdf";
var options = {
  type: "ocr", // perform ocr to get the text within the scanned image
};

const allLines = [];
allLines.push("DATE,INVOICE,INV,CREDIT/DEBIT,BALANCE,INVOICE,INV,AMOUNT");
let callback = (err, text) => {
  if (err) console.log("Error: ", err);
  console.log("Text pages size: ", text.textPages.length);
  for (var i = 1; i < text.textPages.length; i++) {
    let textArray = text.textPages[i].split("\n");
    // console.log("textArray size: ", textArray.length);
    for (let t of textArray) {
      if (t.indexOf("INV ") > -1 && t.split(" ").length > 5) {
        allLines.push(t.replace(/ /g, ","));
        // console.log(t);
      }
    }
  }

  fs.writeFileSync("../../output/ncl.csv", allLines.join("\n"));
  //   console.log("Text: ", text);
};
var processor = pdf_extract(absolute_path_to_pdf, options, function (err) {
  if (err) {
    return callback(err);
  }
});
processor.on("complete", function (data) {
  inspect(data.text_pages, "extracted text pages");
  callback(null, data);
});
processor.on("error", function (err) {
  inspect(err, "error while extracting pages");
  return callback(err);
});

// must turn -psm into --psm in pdf-ocr module
