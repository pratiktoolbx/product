var pdfreader = require("pdfreader");
var fs = require("fs");

(async () => {
  const filename = "../../pdfs/dtl_may.pdf";
  const allLines = [];
  let page = 0;
  allLines.push("TYPE,NUMBER,DATE,AMOUNT,BALANCE");
  try {
    new pdfreader.PdfReader().parseFileItems(filename, function (err, item) {
      if (item && page < 15) {
        // end of file, or page
        //   console.log(renderMatrix(table.getMatrix()));
        if (item.page) {
          console.log("PAGE:", item.page);
          page = item.page;
          console.log();
          page = item.page;
        } else if (item.text && page < 11) {
          let text = item.text.trim().replace(/ +(?= )/g, "");
          let textArray = text.split(" ");
          if (
            textArray.length == 5 &&
            (textArray[0] == "SALE" || textArray[0] == "RETRN")
          ) {
            allLines.push(text.replace(/ /g, ","));
            console.log("text: ", text.split(" ").length, text);
          }
        }

        // resolve(null);
      } else if (!item) {
        console.log("Writing to file NOW");
        fs.writeFileSync("../../output/dtl_may.csv", allLines.join("\n"));
        console.log("DONE NOW");
      }
    });
  } catch (e) {
    console.log("Error trying check file type: ", e);
    return null;
  }
})();
