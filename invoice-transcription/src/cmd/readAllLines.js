var pdfreader = require("pdfreader");

(async () => {
  const filename = "../../pdfs/rona_may.pdf";
  try {
    new pdfreader.PdfReader().parseFileItems(filename, function (err, item) {
      if (!item || item.page) {
        // end of file, or page
        //   console.log(renderMatrix(table.getMatrix()));
        if (item) {
          console.log("PAGE:", item.page);
          page = item.page;
        }

        //   table = new pdfreader.TableParser(); // new/clear table for next page
      } else if (item.text) {
        let text = item.text.trim();
        console.log("text: ", text);

        // resolve(null);
      }
    });
  } catch (e) {
    console.log("Error trying check file type: ", e);
    return null;
  }
})();
