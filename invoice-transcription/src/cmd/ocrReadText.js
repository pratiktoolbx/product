var inspect = require("eyes").inspector({ maxLength: 20000 });
var pdf_extract = require("pdf-ocr");
var absolute_path_to_pdf = "../../pdfs/DTL_TB-24524.pdf";
// var absolute_path_to_pdf = "./pdfs/NCL_TB-24333.pdf";
// var absolute_path_to_pdf = "./pdfs/RONA_TB_25549.pdf";
// var absolute_path_to_pdf = "./pdfs/RONA_TB-25641.pdf";
var options = {
  type: "text", // perform ocr to get the text within the scanned image
};

let callback = (err, text) => {
  if (err) console.log("Error: ", err);
  console.log("Text: ", text);
};
var processor = pdf_extract(absolute_path_to_pdf, options, function (err) {
  if (err) {
    return callback(err);
  }
});
processor.on("complete", function (data) {
  inspect(data.text_pages, "extracted text pages");
  callback(null, data);
});
processor.on("error", function (err) {
  inspect(err, "error while extracting pages");
  return callback(err);
});

// must turn -psm into --psm in pdf-ocr module
