const template = {
  total: null,
  subtotal: null,
  tax: null,
  products: [],
};

module.exports = {
  template,
};
