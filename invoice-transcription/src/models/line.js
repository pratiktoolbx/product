const template = {
  productName: "",
  sku: null,
  qty: null,
  price: null,
  total: null,
};

module.exports = {
  template,
};
