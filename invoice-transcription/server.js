const restify = require("restify");
const re = require("restify-errors");
require("dotenv").config();

const parseHandler = require("./src/parseHandler");

const respond = (req, res, next) => {
  res.send("hello " + req.params.name);
  next();
};

const respondParse = async (req, res, next) => {
  try {
    let pdfURL = req.query.pdfURL;
    let supplier = req.query.supplier;
    if (!pdfURL) return res.send(re.BadRequestError("No PDF URL Found"));
    if (!supplier) return next(re.BadRequestError("No Supplier Found"));

    let result = await parseHandler.handleParse(supplier, pdfURL);
    if (!result) {
      res.send(
        re.InternalServerError("Couldn't handle PDF download and parsing")
      );
    } else if (result.products.length > 0) {
      //   console.log("Got products", result.products.length);
      res.send(result);
    } else {
      res.send(re.NotFoundError("No products found"));
    }
  } catch (e) {
    res.send(e);
  }
  next();
};

const server = restify.createServer();
server.use(restify.plugins.queryParser());

server.get("/hello/:name", respond);
server.head("/hello/:name", respond);

server.post("/parse", respondParse);
let port = process.env.PORT;

server.listen(port, function () {
  console.log("%s listening at %s", server.name, server.url);
});
