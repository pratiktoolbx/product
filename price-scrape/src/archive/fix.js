const fs = require("fs");

const OUTFILE = "./out.csv";
let outstring = fs.readFileSync(OUTFILE, "utf-8");

let strings = outstring.split("\n");
let count = 0;
let rows = [];
for (let s of strings) {
  let arr = s.split(",");
  let last = arr.slice(Math.max(arr.length - 6, 0));
  let uuid = arr[2];
  let name = arr.slice(3, arr.length - 6).join(",");

  let row = [uuid, name];
  row.push(...last);
  fs.appendFileSync("fixedOut.csv", "\n");
  fs.appendFileSync("fixedOut.csv", row.join("|"));
  //   console.log("ROw: ", row);

  //   console.log("length, name: ", arr.length, name);
  count = count + 1;

  //   if (count > 20) break;
}
console.log("Done rows; ", count);
