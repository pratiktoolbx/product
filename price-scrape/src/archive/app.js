const { GoogleSpreadsheet } = require("google-spreadsheet");
const axios = require("axios");
const moment = require("moment");
const creds = require("./google-cred.json");
const fs = require("fs");
const csv = require("csv-parser");
const OUTFILE = "./out.csv";

// spreadsheet key is the long id in the sheets URL
function sleep(milliseconds) {
  const date = Date.now();
  let currentDate = null;
  do {
    currentDate = Date.now();
  } while (currentDate - date < milliseconds);
}

const loadNCLSKUs = async () => {
  let results = [];
  await new Promise((resolve, reject) => {
    fs.createReadStream("./ncl.csv")
      .pipe(csv())
      .on("data", (data) => results.push(data))
      .on("end", () => {
        resolve(results);
      });
  });

  return results;
};

const getHDResponse = async (sku) => {
  let stores = ["7073", "7129", "7012", "7073"];
  for (let store of stores) {
    try {
      let response = await axios.get(
        `https://www.homedepot.ca/homedepotcacommercewebservices/v2/homedepotca/products/${sku}/localized/${store}?fields=BASIC_SPA&lastAddedFulfillment=SHIPTOHOME&lang=en`
      );
      if (response && response.data && response.data.errors === undefined) {
        return response;
      }
    } catch (e) {
      console.log("HD Error - couldn't find SKU at store: ", sku, store);
    }
  }
  return null;
};

const getHD = async (sku) => {
  let price = null;
  let found = null;
  let quantity = null;
  try {
    if (sku.length < 2) {
      return { price: null, found: false, quantity: "Missing SKU" };
    }
    console.log("getting data: ", sku);
    let response = await getHDResponse(sku);
    if (response && response.data && response.data.errors === undefined) {
      let data = response.data;

      //   console.log("Data; ", data);
      if (data.optimizedPrice.regprice) {
        price = data.optimizedPrice.regprice.value;
        found = true;
        quantity = data.fulfillmentMessages.findInStore.storeStockLevel;
      } else {
        found = false;
        quantity = 0;
        price = null;
      }
    } else {
      found = false;
      quantity = "Incorrect SKU";
      price = null;
    }
    return { price, found, quantity };
  } catch (e) {
    console.log("HD Error - couldn't find SKU: ", sku);
  }
  return { price, found: false, quantity: 0 };
};
(async () => {
  try {
    const doc = new GoogleSpreadsheet(
      "1didmsC08Yx4B6nOhnL80GW8XvROR9qapt-I-laEoq4w"
    );
    let auth = await doc.useServiceAccountAuth(creds);

    await doc.loadInfo(); // loads document properties and worksheets
    console.log("DOc title: ", doc.title);
    // await doc.updateProperties({ title: "renamed doc" });

    doc.sheetsByIndex[0]; // or use doc.sheetsById[id]

    const sheet = doc.sheetsById[373267161]; //"Current"
    console.log("Title: ", sheet.title);
    console.log("Rows: ", sheet.rowCount);

    let rows = await sheet.getRows();

    let updatedRows = [];
    const newSheet = await doc.addSheet({
      title: moment().format("YYYYMMDD-kkmmss"),
    });

    fs.appendFileSync(
      OUTFILE,
      [
        "P_ID",
        "P_NAME",
        "P_PRICE",
        "P_SKU",
        "FOUND",
        "NEW_PRICE",
        "QUANTITY",
        "PRICE_PCT_DIFF",
      ].join("|")
    );
    // await newSheet.setHeaderRow([
    //   "P_ID",
    //   "P_NAME",
    //   "P_PRICE",
    //   "P_SKU",
    //   "FOUND",
    //   "NEW_PRICE",
    //   "QUANTITY",
    //   "PRICE_PCT_DIFF",
    // ]);

    for (let r of rows) {
      let newRow = {
        P_ID: r.P_ID,
        P_NAME: r.P_NAME,
        P_PRICE: r.P_PRICE,
        P_SKU: r.P_SKU,
        FOUND: null,
        NEW_PRICE: null,
        QUANTITY: null,
        PRICE_PCT_DIFF: null,
      };

      // if (
      //   r.C_NAME.toLowerCase().indexOf("treated") > -1 ||
      //   r.C_NAME.toLowerCase().indexOf(" pt") > -1
      // ) {
      //   console.log("Pressure Trated (Skipping): ", r.C_NAME);
      //   continue;
      // }

      if (
        r.P_SKU !== undefined &&
        r.P_SKU !== "P_SKU" &&
        r.P_SKU.length > 1 &&
        r.P_SKU.indexOf("100") > -1
      ) {
        let { price, found, quantity } = await getHD(r.P_SKU);
        // console.log("pfq", price, found, quantity);
        if (found) {
          newRow.NEW_PRICE = price;
          newRow.PRICE_PCT_DIFF = (price - r.P_PRICE) / r.P_PRICE;
        } else {
          newRow.NEW_PRICE = r.P_PRICE;
          newRow.PRICE_PCT_DIFF = 0;
        }
        newRow.QUANTITY = quantity;
        newRow.FOUND = found;

        // sleep(1000);
        sleep(Math.floor(Math.random() * (100 - 50)) + 50);
        // break;
      } else {
        newRow.NEW_PRICE = r.P_PRICE;
        newRow.PRICE_PCT_DIFF = 0;
        newRow.QUANTITY = "Incorrect SKU";
        newRow.FOUND = false;
      }
      // updatedRows.push(newRow);

      let newRowArray = [
        newRow.P_ID,
        newRow.P_NAME,
        newRow.P_PRICE,
        newRow.P_SKU,
        newRow.FOUND,
        newRow.NEW_PRICE,
        newRow.QUANTITY,
        newRow.PRICE_PCT_DIFF,
      ];
      fs.appendFileSync(OUTFILE, "\n");
      fs.appendFileSync(OUTFILE, newRowArray.join("|"));
      // await newSheet.addRows([newRow]);
      //   break;
      //
    }
    // console.log("rowscount: ", rows.length);

    // await newSheet.loadCells("J2:J");
    // for (let i = 0; i < updatedRows.length; i++) {
    //   let c = await newSheet.getCell(i + 1, 9);
    //   console.log("C VALUE: ", c.value);
    //   c.numberFormat = { type: "PERCENT", pattern: "#.##%" };
    // }
    // await newSheet.saveUpdatedCells();

    console.log("done");
  } catch (e) {
    console.log("error: with google sheets", e);
  }
})();
