const postgres = require("postgres");
//https://www.npmjs.com/package/postgres

const sql = postgres(
  "postgres://toolbx_master:MFXB;n23qmth{Rw8aAZ{Q}kp3xS&drMX+eRpR2YhxN3$;4j,UOrguP554xLX^rsG@production-db.c9sgfrlcftod.us-east-1.rds.amazonaws.com/toolbx",
  {}
);

function escapeString(str) {
  return str.replace(/[\0\x08\x09\x1a\n\r"'\\\%]/g, function (char) {
    switch (char) {
      case "\0":
        return "\\0";
      case "\x08":
        return "\\b";
      case "\x09":
        return "\\t";
      case "\x1a":
        return "\\z";
      case "\n":
        return "\\n";
      case "\r":
        return "\\r";
      case '"':
      case "'":
      case "\\":
      case "%":
        return "\\" + char; // prepends a backslash to backslash, percent,
      // and double/single quotes
      default:
        return char;
    }
  });
}

// const sql = postgres('postgres://username:password@host:port/database', {
//   host        : '',         // Postgres ip address or domain name
//   port        : 5432,       // Postgres server port
//   path        : '',         // unix socket path (usually '/tmp')
//   database    : '',         // Name of database to connect to
//   username    : '',         // Username of database user
//   password    : '',         // Password of database user
//   ssl         : false,      // True, or options for tls.connect
//   max         : 10,         // Max number of connections
//   timeout     : 0,          // Idle connection timeout in seconds
//   types       : [],         // Array of custom types, see more below
//   onnotice    : fn          // Defaults to console.log
//   onparameter : fn          // (key, value) when server param change
//   debug       : fn          // Is called with (connection, query, parameters)
//   transform   : {
//     column            : fn, // Transforms incoming column names
//     value             : fn, // Transforms incoming row values
//     row               : fn  // Transforms entire rows
//   },
//   connection  : {
//     application_name  : 'postgres.js', // Default application_name
//     ...                                // Other connection parameters
//   }
// })
module.exports = {
  sql,
  escapeString,
};
// const sql = postgres({ ...options }); // will default to the same as psql
