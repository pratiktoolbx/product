const jsforce = require("jsforce");
require("dotenv").config();

let conn = new jsforce.Connection({
  oauth2: {
    clientId: process.env.SALESFORCE_CLIENT_ID,
    clientSecret: process.env.SALESFORCE_CLIENT_SECRET,
    loginUrl: process.env.SALESFORCE_LOGIN_URI,
    redirectUri: process.env.SALESFORCE_REDIRECT_URI,
  },
});
conn.bulk.pollTimeout = 60000;

const connect = async () => {
  try {
    await conn.login(
      process.env.SALESFORCE_USERNAME,
      process.env.SALESFORCE_PASSWORD
    );
    console.log("Logged In: ", conn.accessToken);
  } catch (e) {
    console.log("Exception when trying to login: ", e);
  }
  return null;
};

const getHistoricalProducts = async () => {
  try {
    let result = await conn.query(
      `SELECT 
      OrderItem.UnitPrice,    
      OrderItem.Unit_Quote__c,
          OrderItem.Unit_Cost__c,
          OrderItem.Product2Id,
          Product2.ExternalId
       FROM 
          OrderItem
       WHERE  
          Order.EffectiveDate = Last_N_Days:10 
          AND Order.Status = 'Paid' 
          AND Order.Type = 'TOOLBX Select' 
          AND OrderItem.Unit_Quote__c > 0 
          AND Product2.ExternalId != 'TOOLBX Delivery'
          AND Product2.ExternalId != 'TOOLBX Discount'`
    );

    // console.log("Material Ordered Length: ", result.records.length);
    return result.records;
  } catch (e) {
    console.error(e);
  }
  return null;
};

module.exports = {
  connect,
  getHistoricalProducts,
};
