const { GoogleSpreadsheet } = require("google-spreadsheet");
const creds = require("../../google-cred.json");

const getNCLPrices = async (products) => {
  try {
    console.log("Getting NCL prices from Onboarding Google Sheet...");
    const TBID_COL = "TOOLBX Equivalent";
    const doc = new GoogleSpreadsheet(
      "1aDi9X3Pt0oTZ1SS097_5khwqj2Gbwp1xFuPNNFXbrMM"
    );
    let auth = await doc.useServiceAccountAuth(creds);

    await doc.loadInfo(); // loads document properties and worksheets
    const sheet = doc.sheetsById[46161481]; //"Products Sheet"
    console.log("Doc: ", doc.title, sheet.title, sheet.rowCount);

    let rows = await sheet.getRows();

    for (let r of rows) {
      if (r[TBID_COL] !== undefined && r[TBID_COL].length > 5) {
        if (!(r[TBID_COL] in products)) {
          continue;
        }

        products[r[TBID_COL]]["ncl"] = parseFloat(r.Price); //.toFixed(2);
      }
    }
  } catch (e) {
    console.log("Error getting NCL prices", e);
  }
};

module.exports = {
  getNCLPrices,
};
