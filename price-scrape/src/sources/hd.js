const cliProgress = require("cli-progress");
const axios = require("axios");
// spread out API checks so as to not bombard the HD API
function sleep(milliseconds) {
  const date = Date.now();
  let currentDate = null;
  do {
    currentDate = Date.now();
  } while (currentDate - date < milliseconds);
}

const getHomeDepotPrices = async (products) => {
  try {
    console.log("Getting HD Prices...");

    // create a new progress bar instance and use shades_classic theme
    const hdProgressBar = new cliProgress.SingleBar(
      {},
      cliProgress.Presets.shades_classic
    );

    let stores = ["7073", "7129", "7012", "7013"];

    let pids = Object.keys(products);

    hdProgressBar.start(pids.length, 0);
    let count = 0;
    for (let pid of pids) {
      let p = products[pid];

      hdProgressBar.update(count);
      count += 1;
      if (p.sku && p.sku.indexOf("100") === 0) {
        for (let store of stores) {
          try {
            let url = `https://www.homedepot.ca/homedepotcacommercewebservices/v2/homedepotca/products/${p.sku}/localized/${store}?fields=BASIC_SPA&lastAddedFulfillment=SHIPTOHOME&lang=en`;

            let response = await axios.get(url);

            if (
              response &&
              response.data &&
              response.data.errors === undefined &&
              response.data.optimizedPrice.regprice
            ) {
              p["hd"] = response.data.optimizedPrice.regprice.value;
            } else {
              p["hd"] = 0;
            }
            break;
          } catch (e) {
            //   console.log("HD Error - couldn't find SKU at store: ", p.sku, store);
          }
        }
        if (p.hd === undefined) {
          p["hd"] = 0;
        }
      }
      sleep(Math.floor(Math.random() * (100 - 50)) + 50);
    }
    hdProgressBar.stop();
  } catch (e) {
    console.log("Error with getting HD prices...", e);
  }
};

module.exports = {
  getHomeDepotPrices,
};
