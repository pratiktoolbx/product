const sf = require("../util/salesforce");

const getHistoricalMax = async (products) => {
  try {
    console.log("Getting historical max price from orders...");
    await sf.connect();

    let records = await sf.getHistoricalProducts();
    console.log("SF Record Length: ", records.length);
    if (records.length > 0) {
      for (let r of records) {
        if (!(r.Product2.ExternalId in products)) {
          // console.log("Not in products; ", r.Product2.ExternalId, (count += 1));
          continue;
        }

        let p = products[r.Product2.ExternalId];

        if (!("historical_max" in p) || isNaN(p.historical_max)) {
          p.historical_max = r.Unit_Quote__c;
        } else {
          p.historical_max = Math.max(p.historical_max, r.Unit_Quote__c);
        }
        // console.log("Product price and max; ", p.historical_max, r.Unit_Quote__c);
      }
    }
  } catch (e) {
    console.log("Error getting SF item", e);
  }
};

module.exports = {
  getHistoricalMax,
};
