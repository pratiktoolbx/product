// FILE LIBS
const fs = require("fs");
const csv = require("csv-parser");

// SOURCES
const db = require("./src/sources/db");
const hd = require("./src/sources/hd");
const historical = require("./src/sources/historical");
const sheets = require("./src/sources/sheets");

// CONSTANTS
const DISCREPANCY_THRESHOLD = 0.25;
const UPDATE_FILE = "./csv/price-update.csv";
const LARGE_DISCREP = "./csv/discrep.csv";

require("dotenv").config();

// CSV Utility

const writeCSVHeaderToFile = (file) => {
  fs.appendFileSync(
    file,
    "P_ID,P_NAME,CURRENT_PRICE,HD_PRICE,NCL_PRICE,HISTORICAL_MAX,NEW_PRICE"
  );
};
const writeProductToCSV = (file, p) => {
  // Update File
  fs.appendFileSync(file, "\n");
  fs.appendFileSync(
    file,
    [
      p.id,
      `"${p.name.replace(/"/g, '""')}"`,
      p.current,
      p.hd,
      p.ncl,
      p.historical_max,
      Math.max(p.hd, p.ncl, p.historical_max),
    ].join(",")
  );
};

const outputToCSV = async (products) => {
  try {
    console.log("Writing to files...");

    // clear files
    fs.writeFileSync(UPDATE_FILE, "");
    fs.writeFileSync(LARGE_DISCREP, "");
    writeCSVHeaderToFile(UPDATE_FILE);
    const largeDiscrepProducts = [];
    let pids = Object.keys(products);

    console.log("Items Length: ", pids.length);
    let count = 0;

    for (let pid of pids) {
      let p = products[pid];
      p["id"] = pid;
      p.hd = isNaN(p.hd) ? 0 : p.hd;
      p.ncl = isNaN(p.ncl) ? 0 : p.ncl;
      p.historical_max = isNaN(p.historical_max) ? 0 : p.historical_max;
      let max = Math.max(p.hd, p.ncl, p.historical_max);

      // No price found between HD, NCL, and Historical. Skip updates.
      // or max is already current listed price
      // if (max === 0 || max === p.current) continue;
      if (max === 0) continue;

      // check for large discrep.

      let maxThreshold = 1 + DISCREPANCY_THRESHOLD;
      let minThreshold = 1 - DISCREPANCY_THRESHOLD;
      if (max / p.current > maxThreshold || max / p.current < minThreshold) {
        largeDiscrepProducts.push(p);
        continue;
      }

      writeProductToCSV(UPDATE_FILE, p);
    }

    writeCSVHeaderToFile(LARGE_DISCREP);
    for (let p of largeDiscrepProducts) {
      writeProductToCSV(LARGE_DISCREP, p);
    }

    return;
  } catch (e) {
    console.log("Error in writing to files: ", e);
  }
  return;
};

// MAIN METHOD
(async () => {
  try {
    const products = await db.getCurrentProductList();
    await hd.getHomeDepotPrices(products);
    await sheets.getNCLPrices(products);
    await historical.getHistoricalMax(products);

    await outputToCSV(products);
    console.log("DONE PROCESSING PRICES");
    return;
  } catch (e) {
    console.log("Error occurrred: ", e);
  }
  return;
})();
